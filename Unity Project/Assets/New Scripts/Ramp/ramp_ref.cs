﻿using UnityEngine;
using System.Collections;

public class ramp_ref : MonoBehaviour {
	
	Camera touchcamera;
	public Camera usecamera;
	private bool _mouseState;
	public GameObject Target;
	public Vector3 screenSpace;
	public Vector3 offset;
	
	// Origem dos limites
	public Cursor cursor;
	private Vector3 initialcoor;
	private Vector3 finalcoor;

	// variavel touch
	private Touch touch;

	//
	private bool set;
	private Vector3 worldpos;

	//
	public New_Movement_controls screen;
	
	// Use this for initialization
	void Start ()
	{
		touchcamera = usecamera;
		set = true;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (set) {
			worldpos = transform.position;
			set = false;
		}
		CheckPos ();

		// Debug.Log(_mouseState);
		if (TuioInput.touchCount > 0) {
			touch = TuioInput.touches [0];
		}
		
		if (touch.phase == TouchPhase.Began && screen.unlocked == true) {
			RaycastHit hitInfo;
			if (Target == GetClickedObject (out hitInfo)) {
				_mouseState = true;
				screenSpace = touchcamera.WorldToScreenPoint (Target.transform.position);
				offset = Target.transform.position - touchcamera.ScreenToWorldPoint (new Vector3 (touch.position.x, touch.position.y, screenSpace.z));
			}
		}
		if (touch.phase == TouchPhase.Ended) {
			_mouseState = false;
		}
		if (_mouseState) {
			//keep track of the mouse position
			var curScreenSpace = new Vector3 (touch.position.x, touch.position.y, screenSpace.z);
			
			//convert the screen mouse position to world point and adjust with offset
			var curPosition = touchcamera.ScreenToWorldPoint (curScreenSpace) + offset;
			
			//update the position of the object in the world
			Target.transform.position = curPosition;
		}
		CheckPos ();
	}
	
	
	GameObject GetClickedObject (out RaycastHit hit)
	{
		GameObject target = null;
		Ray ray = touchcamera.ScreenPointToRay (touch.position);
		if (Physics.Raycast (ray.origin, ray.direction * 10, out hit)) {
			target = hit.collider.gameObject;
		}
		
		return target;
	}
	
	void CheckPos(){
		
		Vector3 newpos = transform.position;
		
		if (transform.position.x != worldpos.x) {
			newpos.x = worldpos.x;
		}

		if (transform.position.y != worldpos.y) {
			newpos.y = worldpos.y;
		}
		
		if (transform.position.z < cursor.initialcoor.z) {
			newpos.z = cursor.initialcoor.z;
		}
		
		if (transform.position.z > cursor.finalcoor.z) {
			newpos.z = cursor.finalcoor.z;
		}
		
		if (newpos != transform.position) {
			transform.position = newpos;
		}
		
	}
}
