﻿using UnityEngine;
using System.Collections;

public class Manage_ramp : MonoBehaviour {

	
	// Origem das medidas da camara
	public Cursor cursor;
	
	// Camera variables
	public Camera touchcamera;
	// Medidas da camara para normalizaçao
	public float screen_lenght;
	public float screen_height;
	
	// Normalization measurementes
	Vector3 centralpixel;
	
	// Mediçoes de ecra
	public Vector3 initialcoor;
	public Vector3 finalcoor;
	private float plane_lenght;
	private float plane_height;
	
	// Mediçoes dos rectangulos
	private float rect_lenght;
	private float rect_height;
	
	// Rectangulos a manipular
	private GameObject[] refs;
	public GameObject ref_1;
	public GameObject ref_2;
	public GameObject ref_3;
	public GameObject ref_4;
	public GameObject ref_5;
	public GameObject ref_6;
	public GameObject ref_7;
	public GameObject ref_8;
	public GameObject ref_9;
	public GameObject ref_10;
	public GameObject ref_11;

	// Posiçoes em z de cada rectangulo
	private float[] worldz;
	private float[] scaledval;
	public float z_min;
	public float z_max;
	
	// Funçao de tranferencia
	public float[] trans_func;
	
	// Sistema de particulas
	public GameObject grapher;
	
	// CUstom Function
	public CustomFunction customs;
	
	void Start () {
		
		// define as medidas da tela
		screenBounds ();
		
		// inicializa os vectores
		trans_func = new float[256];
		refs = new GameObject[11];
		worldz= new float[11];
		scaledval = new float[11];
		
		refs [0] = ref_1;
		refs [1] = ref_2;
		refs [2] = ref_3;
		refs [3] = ref_4;
		refs [4] = ref_5;
		refs [5] = ref_6;
		refs [6] = ref_7;
		refs [7] = ref_8;
		refs [8] = ref_9;
		refs [9] = ref_10;
		refs [10] = ref_11;


		z_max = finalcoor.z;
		z_min = initialcoor.z;
		
		rect_lenght = plane_lenght/ 10f;
		rect_height = plane_height;

		// Define o tamanho e posiçoes dos rectangulos
		Set_measures ();
	
		// Define um valor entre 0 e 1 tendo em conta a posiçao dos rectangulos
		Scale ();
		
		// Define a funçaod e transferencia para os vectores
		//Transfer_func ();
		//Adapt_1D ();
	}
	
	void Update () {
		
		float[] newpos = new float[11];
		bool changed = false;
		
		for (int i=0; i < newpos.Length; i++) {
			newpos[i] = refs[i].transform.position.z;
			
			if(worldz[i]!= newpos[i]){
				changed = true;
			}
		}
		
		if (changed) {
			worldz = newpos;
			Scale();
			Transfer_func();
			grapher.SendMessage ("Apply_function", trans_func , SendMessageOptions.DontRequireReceiver);
			//print ("Changed");
		}
		
	}
	
	void Set_measures(){

		for (int i=0; i < refs.Length; i++) {
			
			Vector3 worldpos = new Vector3 (initialcoor.x + rect_lenght*i, 3f, initialcoor.z);
			refs[i].transform.position = worldpos;
			
			worldz[i]= worldpos.z;
		}
	}
	
	void screenBounds () {
		
		Vector3 screenPosfi = touchcamera.WorldToScreenPoint(touchcamera.transform.position);
		
		centralpixel = screenPosfi;
		
		screen_height = touchcamera.pixelHeight;
		screen_lenght = touchcamera.pixelWidth;
		
		initialcoor = touchcamera.ScreenToWorldPoint (new Vector3(centralpixel.x - screen_lenght / 2, centralpixel.y - screen_height / 2, 0f));
		finalcoor = touchcamera.ScreenToWorldPoint (new Vector3(centralpixel.x + screen_lenght / 2, centralpixel.y + screen_height / 2, 0f));
		
		plane_height = finalcoor.z - initialcoor.z;
		plane_lenght = finalcoor.x - initialcoor.x;
		
	}
	
	void Scale(){
		
		for (int i=0; i< worldz.Length; i++) {
			
			float cval = worldz[i] - z_min;
			
			scaledval[i] = cval / rect_height;
		}
		
	}
	
	void Transfer_func(){
		
		float div = 255f / 10f;
		
		for (int i = 0; i< scaledval.Length -1; i++) {
			
			int one = Mathf.RoundToInt(i*div);
			int two = Mathf.RoundToInt((i+1)*div);

			for(int j= one; j<= two; j++){
				float t = (j-one)/div;
				trans_func[j] = Mathf.Lerp(scaledval[i], scaledval[i+1], t);
			}
		}

		customs.funcao_transferencia = trans_func;
	}
	
	void Adapt_1D (){

		//declaracao do vector que importei do cursor
		float[] transf_vector = customs.funcao_transferencia;

		Vector3 pos_1 = refs [0].transform.position;
		pos_1.z = initialcoor.z + (transf_vector[0] * rect_height);
		refs [0].transform.position = pos_1;

		Vector3 pos_2 = refs [10].transform.position;
		pos_2.z = initialcoor.z + (transf_vector[255] * rect_height);
		refs [10].transform.position = pos_2;

		//Intervalos de intensidades
		float div = 255f / 10f;
		
		for (int i = 1; i< scaledval.Length - 2; i++) {
			
			int one = Mathf.RoundToInt(i*div);
			int two = Mathf.RoundToInt((i+2)*div);
			
			float total = 0;
			float nr = 0;
			for(int j= one; j<= two; j++){
				if(transf_vector[j]!=0){
					total = total + transf_vector[j];
					nr = nr + 1f;
				}
			}
			
			float mean = total / nr;
			if (mean <= 1f && mean >= 0f) {
			} else {
				mean=0f;
			}
			Vector3 pos = refs [i].transform.position;
			pos.z = initialcoor.z + (mean * rect_height);
			refs [i].transform.position = pos;
		}

	}

}
