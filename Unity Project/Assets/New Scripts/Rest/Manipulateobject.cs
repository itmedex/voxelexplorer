﻿using UnityEngine;
using System.Collections;

public class Manipulateobject : MonoBehaviour {


	public float rotate_threshold = 0;
	public float distinction_threshold = 4;
	public float second_distinction = 2;

	// Posiçao inicial dos dedos
	private Vector2 startPos;
	private Vector2 startPos_2;
	private Vector2 startPos_3;

	// Direcçao do primeiro dedo
	private Vector2 direction;
	private Vector2 direction_2;
	private Vector2 direction_3;

	// Distancia entre os dedos
	private float mutual_distance;
	private float distance;
	
	// Target
	public GameObject target;
	private Vector3 initialpos;
	private Quaternion initialrot;
	public Grapher5Up grapher;

	//Sensibilidades

	public float rot_sentivity = 1;
	public float zoom_sensitivity = 1;
	public float trans_sensitivity = 1;

	// Bloqueio de ecra
	public bool unlocked = true;

	// Screen var
	private Vector3 initialcoor;
	private Vector3 finalcoor;
	private float plane_height;
	private float plane_lenght;
	private float screen_height;
	private float screen_lenght;

	// Variaveis para taps
	float check_time;
	int touches;
	public GameObject canvas;

	// Rotaçao do multiview
	private bool first = true;
	GameObject[] multy;

	// Box widjet
	public CameraWidget widget;

	//touch camera
	public Function_draw touchcamera;
	private bool move= true;

	// Use this for initialization
	void Start () {

		initialpos = target.transform.position;
		initialrot = target.transform.rotation;
	
		screenBounds ();

		first = true;

		widget.OnCameraChanged += widget_OnCameraChanged;
	}

	void screenBounds () {

		Camera cam = GetComponent<Camera>();

		Vector3 screenPosfi = cam.WorldToScreenPoint(transform.position);
		
		Vector3 centralpixel = screenPosfi;
		
		screen_height = cam.pixelHeight;
		screen_lenght = cam.pixelWidth;

		
		initialcoor = cam.ScreenToWorldPoint (new Vector3(centralpixel.x - screen_lenght / 2, centralpixel.y - screen_height / 2, transform.position.y));
		finalcoor = cam.ScreenToWorldPoint (new Vector3(centralpixel.x + screen_lenght / 2, centralpixel.y + screen_height / 2, transform.position.y));
		
		plane_height = finalcoor.z - initialcoor.z;
		plane_lenght = finalcoor.x - initialcoor.x;
		

	}


	void widget_OnCameraChanged(Camera camera, string view, Vector3 directionn)
	{
//		Debug.Log("Registered " + view + "(" + directionn + ") on " + camera.name);
		//rigidbody.AddForce(direction * 100f);
		/*
		target.transform.LookAt (target.transform.position + directionn);

		foreach(GameObject uni in multy){
			uni.transform.LookAt (uni.transform.position + directionn);
		}
		*/

		if (directionn == Vector3.forward) {
			target.transform.localEulerAngles = new Vector3 (0, 0, 0);

			foreach (GameObject uni in multy) {
				uni.transform.localEulerAngles = new Vector3 (0, 0, 0);
			}
		}

		if (directionn == Vector3.up) {
			target.transform.localEulerAngles = new Vector3 (270f, 0, 0);
				
			foreach (GameObject uni in multy) {
				uni.transform.localEulerAngles = new Vector3 (270f, 0, 0);
			}
		}

		if (directionn == Vector3.down) {
			target.transform.localEulerAngles = new Vector3 (90f, 0, 0);
			
			foreach (GameObject uni in multy) {
				uni.transform.localEulerAngles = new Vector3 (90f, 0, 0);
			}
		}

		if (directionn == Vector3.left) {
			target.transform.localEulerAngles = new Vector3 (0, 0, 90f);
			
			foreach (GameObject uni in multy) {
				uni.transform.localEulerAngles = new Vector3 (0, 0, 90f);
			}
		}

		if (directionn == Vector3.right) {
			target.transform.localEulerAngles = new Vector3 (0, 0, 270f);
			
			foreach (GameObject uni in multy) {
				uni.transform.localEulerAngles = new Vector3 (0, 0, 270f);
			}
		}

		if (directionn == Vector3.back) {
			target.transform.localEulerAngles = new Vector3 (0, 0, 180f);
			
			foreach (GameObject uni in multy) {
				uni.transform.localEulerAngles = new Vector3 (0, 0, 180f);
			}
		}

	}

	void Update () {

		if (first) {

			multy = GameObject.FindGameObjectsWithTag("multigraph");
			first = false;
		
		}

		// Este codigo instancia o pequeno indicador na posiçao do toque
		if (TuioInput.touchCount > 0) {

			foreach( Touch touch in TuioInput.touches){

				if (touch.phase == TouchPhase.Began){
				GameObject dot = (GameObject)Instantiate (Resources.Load ("touched"));
				dot.transform.SetParent(canvas.transform, false);
				Vector3 dotpos = new Vector3 (touch.position.x, touch.position.y, 10f);
				dot.transform.position = dotpos;

				DestroyObject(dot,0.5f);
				}
			}
		}

		if (unlocked) {

			if (TuioInput.touchCount > 0) {

				// Comandos de traslaçao
				Touch touch = TuioInput.touches [0];

				move = true;
				if(touch.position.y > touchcamera.top + 0.04f * screen_height){
					move = true;
				}else{
					move = false;
				}
	
				if(move){

				switch (touch.phase) {
				// Record initial touch position.
				case TouchPhase.Began:
					startPos = touch.position;

					// Controlo de taps - tapCount nao funciona, vou ver se ha soluçao mas por agora uso isto
					// Detecçao do numero de taps
					if (Time.time > check_time + 0.5f){
						touches = 1;
						check_time = Time.time;
					}else{
						touches = touches + 1;
						check_time = Time.time;
					}

					//print (touches);

					if (touches == 3){
						//print ("reset pos");
						target.transform.position = initialpos;
						target.transform.rotation = initialrot;
						foreach(GameObject uni in multy){
							uni.transform.localRotation = initialrot;
						}
						gameObject.GetComponent<Camera>().fieldOfView = 60;
						//grapher.Reset_pos();
					}
					if (touches == 2){
						target.GetComponent<BoundBoxes_BoundBox>().enabled = !target.GetComponent<BoundBoxes_BoundBox>().enabled;
					}
					break;
				
				// Determine direction by comparing the current touch position with the initial one.
				case TouchPhase.Moved:

					direction = touch.position - startPos;

					if (TuioInput.touchCount == 2) {
					
						Touch touch_2 = TuioInput.touches [1];
					
						switch (touch_2.phase) {
						// Record initial touch position.
						case TouchPhase.Began:
							startPos_2 = touch_2.position;
						
							Vector2 dist = startPos - startPos_2;
							mutual_distance = dist.magnitude;
							break;
						
						// Determine direction by comparing the current touch position with the initial one.
						case TouchPhase.Moved:
							direction_2 = touch_2.position - startPos_2;
						
							if( direction.magnitude < 5){

								//  Rotaçao em y - so dois dedos
								
								Vector2 one = touch_2.position - touch.position;
								Vector2 two = startPos_2 - startPos;
								
								float ang = Vector2.Angle (one, two);
								Vector3 cross = Vector3.Cross (one, two);
								
								Camera camera = GetComponent<Camera> ();
								Vector3 worldpivot = camera.ScreenToWorldPoint (new Vector3 (touch.position.x, touch.position.y, transform.position.y));
								worldpivot.y = target.transform.position.y;

								//print (ang);
								if(ang < 10){
									if (cross.z > 0) {
									target.transform.RotateAround (worldpivot, Vector3.up, ang);
								} else {
									target.transform.RotateAround (worldpivot, Vector3.down, ang);
								}
								}
								
								startPos_2 = touch_2.position;
							}

							// Tirar a rotaçao em x e y daqui e meter com tres dedos
							if (Mathf.Abs (direction.magnitude - direction_2.magnitude) < distinction_threshold) {
							
								Vector2 conector = touch_2.position - touch.position;
								distance = conector.magnitude;
							
								if (Mathf.Abs (distance - mutual_distance) < second_distinction) {
									/*
									// Rotaçao em x
									if (Mathf.Abs (direction_2.y) > rotate_threshold) {
										target.transform.Rotate ((direction_2.y/screen_height)* 300f * rot_sentivity, 0, 0, Space.World);
									}
									// Rotaçao em z
									if (Mathf.Abs (direction_2.x) > rotate_threshold) {
										target.transform.Rotate (0, 0, -(direction_2.x/screen_lenght)* 300f * rot_sentivity, Space.World);
									}*/
								}else{
							
								// Ver melhor como mexer na sensibilidade do zoom
								float deviance = (distance - mutual_distance)* 0.2f/screen_lenght;
								//print (deviance * 100);

								//print (distance - mutual_distance);
								if(Mathf.Abs(deviance * 100) < 1f){
								// Zoom in
										// mudei o distance - mutual_distance pelo (deviance*50*90)
										if ((deviance*50*90) > 3f && gameObject.GetComponent<Camera>().fieldOfView > 0) {
									//target.transform.Translate (Vector3.up * deviance * zoom_sensitivity, Space.World);
											gameObject.GetComponent<Camera>().fieldOfView = gameObject.GetComponent<Camera>().fieldOfView  - (deviance*50*45);
								}
								// Zoom out
										if ((deviance*50*90) <  -3f && gameObject.GetComponent<Camera>().fieldOfView < 180) {
									//target.transform.Translate (Vector3.down * - deviance * zoom_sensitivity, Space.World);
											gameObject.GetComponent<Camera>().fieldOfView = gameObject.GetComponent<Camera>().fieldOfView  - (deviance*50*45);
								}
									}
								}
								mutual_distance = distance;
							}

							//mutual_distance = distance;
							break;
						
						case TouchPhase.Ended:
							break;
						}

						//mutual_distance = distance;
						startPos_2 = touch_2.position;
					} 
					if (TuioInput.touchCount > 2){
						
						Touch touch_3 = TuioInput.touches [2];
						
						switch (touch_3.phase) {
							// Record initial touch position.
						case TouchPhase.Began:
							startPos_3 = touch_3.position;
							break;
							
						case TouchPhase.Moved:
							direction_3 = touch_3.position - startPos_3;


							// Rotaçao em x
							if (Mathf.Abs (direction_3.y) > rotate_threshold) {
								if((direction_3.y/screen_height)* 360f * rot_sentivity < 10 && (direction_3.y/screen_height)* 360f * rot_sentivity > - 10){ 
									//print ((direction_3.y/screen_height)* 360f * rot_sentivity );
									target.transform.Rotate ((direction_3.y/screen_height)* 360f * rot_sentivity, 0, 0, Space.World);

									foreach(GameObject uni in multy){
										uni.transform.localRotation = target.transform.localRotation;
									}
								}
							}
							// Rotaçao em z
							if (Mathf.Abs (direction_3.x) > rotate_threshold) {
								if((direction_3.x/screen_height)* 360f * rot_sentivity < 10 && (direction_3.x/screen_height)* 360f * rot_sentivity > - 10 ){
								target.transform.Rotate (0, 0, -(direction_3.x/screen_lenght)* 360f * rot_sentivity, Space.World);

									foreach(GameObject uni in multy){
										uni.transform.localRotation = target.transform.localRotation;
									}
								}
							}

							startPos_3 = touch_3.position;
							break;
						}
						
						
					}else {

						// Translaçao - so para um dedo

						float directionxmove = (direction.x /screen_lenght)* plane_lenght;
						float directionymove = (direction.y / screen_height) * plane_height;


						if( Mathf.Abs (directionxmove) < plane_lenght/2f || Mathf.Abs (directionymove) < plane_height/2f){
						target.transform.Translate (Vector3.right * directionxmove * trans_sensitivity, Space.World);
						target.transform.Translate (Vector3.forward * directionymove * trans_sensitivity, Space.World);
						}
					}

					break;
				
				// Report that a direction has been chosen when the finger is lifted.
				case TouchPhase.Stationary:

					if (TuioInput.touchCount > 1) {
					
						Touch touch_2 = TuioInput.touches [1];
					
						switch (touch_2.phase) {
						// Record initial touch position.
						case TouchPhase.Began:
							startPos_2 = touch_2.position;
							break;
						
						// Determine direction by comparing the current touch position with the initial one.
						case TouchPhase.Moved:
							direction_2 = touch_2.position - startPos_2;
						
							//  Rotaçao em y

							Vector2 one = touch_2.position - touch.position;
							Vector2 two = startPos_2 - startPos;
							
							float ang = Vector2.Angle (one, two);
							Vector3 cross = Vector3.Cross (one, two);
							
							Camera camera = GetComponent<Camera> ();
							touch = TuioInput.touches[0];
							Vector3 worldpivot = camera.ScreenToWorldPoint (new Vector3 (touch.position.x, touch.position.y, transform.position.y));
							worldpivot.y = target.transform.position.y;

							if(ang < 10){
							if (cross.z > 0) {
								target.transform.RotateAround (worldpivot, Vector3.up, ang);

										foreach(GameObject uni in multy){
											uni.transform.localRotation = target.transform.localRotation;
										}
							} else {
								target.transform.RotateAround (worldpivot, Vector3.down, ang);

										foreach(GameObject uni in multy){
											uni.transform.localRotation = target.transform.localRotation;
										}
							}
							}
							startPos_2 = touch_2.position;
							break;
						
						// Report that a direction has been chosen when the finger is lifted.
						case TouchPhase.Ended:
							break;
						}
					}
					break;
				}

				startPos = touch.position;
			}
			}
		}
	}
}
