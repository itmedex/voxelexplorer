﻿using UnityEngine;
using System.Collections;

public class RectControl_2 : MonoBehaviour {
	
	Camera touchcamera;
	public Camera usecamera;
	private bool _mouseState;
	public GameObject Target;
	public Vector3 screenSpace;
	public Vector3 offset;
	
	// Posiçao de refrencia
	private bool set = true;
	private Vector3 worldpos;

	// variavel touch
	private Touch touch;

	public Manipulateobject screen;
	public ManageRects manage;
	private float zmax;
	private float zmin;
	
	// Use this for initialization
	void Start ()
	{
		touchcamera = usecamera;

	}
	
	// Update is called once per frame
	void Update ()
	{
		if (set) {
			worldpos = transform.position;
			set = false;
			zmax = manage.z_max;
			zmin = manage.z_min;
		}
		
		// Debug.Log(_mouseState);
		if (TuioInput.touchCount > 0) {
			touch = TuioInput.touches [0];
		}

		if (touch.phase == TouchPhase.Began) {
			RaycastHit hitInfo;
			if (Target == GetClickedObject (out hitInfo)) {
				_mouseState = true;
				screen.unlocked = false;
				screenSpace = touchcamera.WorldToScreenPoint (Target.transform.position);
				offset = Target.transform.position - touchcamera.ScreenToWorldPoint (new Vector3 (touch.position.x, touch.position.y, screenSpace.z));
			}
		}
		if (touch.phase == TouchPhase.Ended) {
			_mouseState = false;
			screen.unlocked = true;
		}
		if (_mouseState) {
			//keep track of the mouse position
			var curScreenSpace = new Vector3 (touch.position.x, touch.position.y, screenSpace.z);
			
			//convert the screen mouse position to world point and adjust with offset
			var curPosition = touchcamera.ScreenToWorldPoint (curScreenSpace) + offset;
			
			//update the position of the object in the world
			Target.transform.position = curPosition;

			// Unico codigo original
			CheckPos ();
		}
	}
	
	
	GameObject GetClickedObject (out RaycastHit hit)
	{
		GameObject target = null;
		Ray ray = touchcamera.ScreenPointToRay (touch.position);
		if (Physics.Raycast (ray.origin, ray.direction * 10, out hit)) {
			target = hit.collider.gameObject;
		}
		
		return target;
	}
	
	void CheckPos(){
		
		Vector3 newpos = transform.position;
		
		if (transform.position.x != worldpos.x) {
			newpos.x = worldpos.x;
		}
		
		
		if (transform.position.y != worldpos.y) {
			newpos.y = worldpos.y;
		}
		
		
		if (transform.position.z > zmax) {
			newpos.z = zmax;
		}
		
		if (transform.position.z < zmin) {
			newpos.z = zmin;
		}
		
		transform.position = newpos;
		
	}
}
