﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Function_draw : MonoBehaviour {


	public LayerMask touchInputMask;
	
	private List<GameObject> touchList = new List<GameObject>();
	private GameObject[] touchesOld;
	
	public GameObject cursor;
	
	public int[] func;
	
	// Toggle Lock
	public bool unlocked = true;
	
	public float xfloat;
	public float yfloat;
	
	private RaycastHit hit;

	public Manipulateobject screen;
	public GameObject sliding_boxes;
	public GameObject sliding_window;
	public GameObject ramp;

	// Screen var
	private Vector3 initialcoor;
	private Vector3 finalcoor;
	private float screen_height;
	private float screen_lenght;
	public float top;
	public float bottom;
	public float right;
	public float left;

	void Start(){
		screenBounds ();
	}

	void Update () {

		if (sliding_boxes.activeSelf || sliding_window.activeSelf || ramp.activeSelf) {
			unlocked = false;
		}
		
		if (unlocked) {

			if (TuioInput.touchCount > 0) {
				
				touchesOld = new GameObject[touchList.Count];
				touchList.CopyTo (touchesOld);
				touchList.Clear ();
				
				
				foreach (Touch touch in TuioInput.touches) {

					xfloat = touch.position.x;
					yfloat = touch.position.y;

					if( true /*xfloat < right && xfloat > left && yfloat < top && yfloat > bottom*/){

					Ray ray = GetComponent<Camera>().ScreenPointToRay (touch.position);
					
					if (Physics.Raycast (ray, out hit, touchInputMask)) {
						
						GameObject recipient = hit.transform.gameObject;
						touchList.Add (recipient);
						
							if (touch.phase == TouchPhase.Began && xfloat < right && xfloat > left && yfloat < top && yfloat > bottom) {
							//recipient.SendMessage ("OnTouchDown", hit.point, SendMessageOptions.DontRequireReceiver);
							cursor.SendMessage ("makeLine", SendMessageOptions.DontRequireReceiver);
							recipient.SendMessage ("Follow", hit.point, SendMessageOptions.DontRequireReceiver);
							screen.unlocked =false;

						}
						if (touch.phase == TouchPhase.Ended) {
							//recipient.SendMessage ("OnTouchUp", hit.point, SendMessageOptions.DontRequireReceiver);
							cursor.SendMessage ("FinishLine", SendMessageOptions.DontRequireReceiver);
							recipient.SendMessage ("Follow", hit.point, SendMessageOptions.DontRequireReceiver);
							screen.unlocked =true;
						}
						if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved) {
							//recipient.SendMessage ("OnTouchStay", hit.point, SendMessageOptions.DontRequireReceiver);
							cursor.SendMessage ("UpdateLine", SendMessageOptions.DontRequireReceiver);
						}
						if (touch.phase == TouchPhase.Canceled) {
							//recipient.SendMessage ("OnTouchExit", hit.point, SendMessageOptions.DontRequireReceiver);
						}
					}
					}
				}
				foreach (GameObject g in touchesOld) {
					if (!touchList.Contains (g)) {
						//g.SendMessage ("OnTouchExit", hit.point, SendMessageOptions.DontRequireReceiver);
					}
				}
			}
		}
	}

	void screenBounds () {
		
		Camera cam = GetComponent<Camera>();
		
		Vector3 screenPosfi = cam.WorldToScreenPoint(transform.position);
		
		Vector3 centralpixel = screenPosfi;
		
		screen_height = cam.pixelHeight;
		screen_lenght = cam.pixelWidth;
		
		top = centralpixel.y + screen_height / 2;
		bottom = centralpixel.y - screen_height / 2;
		left = centralpixel.x - screen_lenght / 2;
		right = centralpixel.x + screen_lenght / 2;
		
		
	}
	
	public void Toggle_Lock (){
		unlocked = !unlocked;
		//print ("lock toggled");
	}
}
