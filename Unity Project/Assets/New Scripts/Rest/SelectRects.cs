﻿using UnityEngine;
using System.Collections;

public class SelectRects : MonoBehaviour {

	// blocos de selecçao
	public Selectionbox selection_1;
	public Selectionbox selection_2;
	public Selectionbox selection_3;
	public Selectionbox selection_4;
	public Selectionbox selection_5;
	public Selectionbox selection_6;
	public Selectionbox selection_7;
	public Selectionbox selection_8;
	public Selectionbox selection_9;

	// Vector de blocos
	private Selectionbox[] select_blocos;

	// Vector de selecçao
	private int[] select;

	void Start () {

		select_blocos = new Selectionbox[9];
		select_blocos[0] = selection_1;
		select_blocos[1] = selection_2;
		select_blocos[2] = selection_3;
		select_blocos[3] = selection_4;
		select_blocos[4] = selection_5;
		select_blocos[5] = selection_6;
		select_blocos[6] = selection_7;
		select_blocos[7] = selection_8;
		select_blocos[8] = selection_9;
	}
	

	void Update () {
	
	}

	void Create_ref(){

		select = new int[9];

		for (int i=0; i < select_blocos.Length; i++) {
			if(select_blocos[i].active){
				select[i] = 1;
			}
		}
	}
}
