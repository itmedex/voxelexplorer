﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Newselect : MonoBehaviour {
	
	public GameObject slidingboxes;
	public GameObject cursor;
	public GameObject slidingwindow;
	public GameObject ramp;
	public Grapher5Up grapher;

	//public Touchscript draw;
	public Cursor cursor_class;
	public Function_draw draw;
	public GameObject turn_back;

	public GameObject free_draw;
	public GameObject slider;
	public GameObject single_slider;
	public GameObject ramp_button;

	public GameObject open_menu2;
	public GameObject close_menu2;

	//
	public Sprite barras;
	private Button button;

	void Start () {

		Select_free ();
		Close_menu2 ();
		Set_Images ();

	}
	
	void Update () 
	{


		if(Input.GetKeyDown(KeyCode.Escape) == true)
		{
			Exitapp();
		}
	}
	
	public void Exitapp(){
		Application.Quit();
	}


	public void Toggle_multiview(){

	}

	public void Selec_menu(){
		GameObject tran = GameObject.Find ("Transition");
		Destroy (tran);
		Application.LoadLevel("Select_model");
	}

	public void Select_free(){
		// Permite mexer no cursor
		draw.unlocked = true;
		cursor_class.adapt_1D = false;
		// Retira os sliders
		slidingboxes.SetActive (false);
		ramp.SetActive (false);
		slidingwindow.SetActive (false);
		//print ("selected free");
	}

	public void Select_segments(){
		//print ("selected slide");
		// Bloqueia o cursor
		draw.unlocked = false;
		//cursor.SendMessage ("erase_line", SendMessageOptions.DontRequireReceiver);
		// Activa os sliders
		ramp.SetActive (false);
		slidingwindow.SetActive (false);
		slidingboxes.SetActive (true);
		slidingboxes.SendMessage ("Adapt_1D", SendMessageOptions.DontRequireReceiver);
		//print ("completed selected slide");
	}

	public void Select_window()
	{
		//print ("selected slide");
		// Bloqueia o cursor
		draw.unlocked = false;
		//cursor.SendMessage ("erase_line", SendMessageOptions.DontRequireReceiver);
		// Desactiva os sliders
		ramp.SetActive (false);
		slidingboxes.SetActive (false);
		slidingwindow.SetActive (true);
		//print ("completed selected slider");
	}

	public void Select_ramp(){
		// Bloqueia o cursor
		draw.unlocked = false;
		//cursor.SendMessage ("erase_line", SendMessageOptions.DontRequireReceiver);
		// Activa os sliders
		slidingwindow.SetActive (false);
		slidingboxes.SetActive (false);
		ramp.SetActive (true);
		ramp.SendMessage ("Adapt_1D", SendMessageOptions.DontRequireReceiver);
		//print ("completed selected ramp");
	}


	

	public void Open_menu2 ()
	{
		ramp_button.SetActive (true);
		free_draw.SetActive(true);
		slider.SetActive(true);
		single_slider.SetActive(true);
		close_menu2.SetActive (true);
		open_menu2.SetActive (false);

	}

	public void Close_menu2 ()
	{
		ramp_button.SetActive (false);
		free_draw.SetActive(false);
		slider.SetActive(false);
		single_slider.SetActive(false);
		close_menu2.SetActive (false);
		open_menu2.SetActive (true);
	}

	public void Adapt_function(){

		Select_free ();
		cursor_class.adapt_1D = true;
	}

	private void Set_Images(){

		button = slider.GetComponent<Button> ();
		button.image.overrideSprite = barras;
	}
}
