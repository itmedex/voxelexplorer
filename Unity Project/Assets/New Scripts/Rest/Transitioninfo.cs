﻿using UnityEngine;
using System.Collections;

public class Transitioninfo : MonoBehaviour {

	public string selected_model;
	public bool first;

	void Awake() {
		DontDestroyOnLoad(transform.gameObject);
		first = true;
	}

	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.Escape) == true)
		{
			Exitapp();
		}
	}

	public void Exitapp(){
		Application.Quit();
	}

	public void Select_tibia(){
		selected_model = "tibia";
		print ("tibia");
		Application.LoadLevel("test");
	}

	public void Select_cranio(){
		selected_model = "cranio";
		print ("cranio");
		Application.LoadLevel("test");
	}

	public void Select_torax(){
		selected_model = "torax";
		print ("torax");
		Application.LoadLevel("test");
	}

	public void Select_tronco(){
		selected_model = "tronco";
		print ("tronco");
		Application.LoadLevel("test");
	}

	public void Select_test_1(){
		selected_model = "teste1";
		print ("teste1");
		Application.LoadLevel("test");
	}

	public void Select_test_2(){
		selected_model = "teste2";
		print ("teste2");
		Application.LoadLevel("test");
	}

	public void Select_test_3(){
		selected_model = "teste3";
		print ("teste3");
		Application.LoadLevel("test");
	}
	public void Check (){
		print("pressed");
	}
}
