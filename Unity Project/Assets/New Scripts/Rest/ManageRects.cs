﻿using UnityEngine;
using System.Collections;

public class ManageRects : MonoBehaviour {

	// Origem das medidas da camara
	public Cursor cursor;

	// Camera variables
	public Camera touchcamera;
	// Medidas da camara para normalizaçao
	public float screen_lenght;
	public float screen_height;

	// Normalization measurementes
	Vector3 centralpixel;
	
	// Mediçoes de ecra
	public Vector3 initialcoor;
	public Vector3 finalcoor;
	private float plane_lenght;
	private float plane_height;

	// Mediçoes dos rectangulos
	public float rect_lenght;
	public float rect_height;

	
	// Rectangulos a manipular
	private GameObject[] rects;
	public GameObject rect_1;
	public GameObject rect_2;
	public GameObject rect_3;
	public GameObject rect_4;
	public GameObject rect_5;
	public GameObject rect_6;
	public GameObject rect_7;
	public GameObject rect_8;
	public GameObject rect_9;
	public GameObject rect_10;

	// Posiçoes em z de cada rectangulo
	private float[] worldz;
	private float[] scaledval;
	public float z_min;
	public float z_max;

	// Funçao de tranferencia
	public float[] trans_func;

	// Sistema de particulas
	public GameObject grapher;

	// CUstom Function
	public CustomFunction customs;
	
	void Start () {

		// define as medidas da tela
		screenBounds ();

		// medidas dos rectangulos
		rect_lenght = plane_lenght / 10f;
		rect_height = plane_height;

		// inicializa os vectores
		trans_func = new float[256];
		rects = new GameObject[10];
		worldz= new float[10];
		scaledval = new float[10];

		rects [0] = rect_1;
		rects [1] = rect_2;
		rects [2] = rect_3;
		rects [3] = rect_4;
		rects [4] = rect_5;
		rects [5] = rect_6;
		rects [6] = rect_7;
		rects [7] = rect_8;
		rects [8] = rect_9;
		rects [9] = rect_10;

		// Define o tamanho e posiçoes dos rectangulos
		Set_measures ();

		z_max = initialcoor.z + (rect_height / 2);
		z_min = initialcoor.z - (rect_height / 2);

		// Define um valor entre 0 e 1 tendo em conta a posiçao dos rectangulos
		Scale ();

		// Define a funçaod e transferencia para os vectores
		//Transfer_func ();
		//Adapt_1D ();
	}

	void Update () {
	
		float[] newpos = new float[10];
		bool changed = false;

		for (int i=0; i < newpos.Length; i++) {
			newpos[i] = rects[i].transform.position.z;

			if(worldz[i]!= newpos[i]){
				changed = true;
			}
		}

		if (changed) {
			worldz = newpos;
			Scale();
			Transfer_func();
			grapher.SendMessage ("Slider_func", SendMessageOptions.DontRequireReceiver);
			//print ("Changed");
		}

	}

	void Set_measures(){

		for (int i=0; i < rects.Length; i++) {

			Vector3 scale;
			scale.x = rect_lenght;
			scale.z = rect_height;
			scale.y = 0.001f;

			rects[i].transform.localScale = scale; 

			Vector3 worldpos = new Vector3 (initialcoor.x + (rect_lenght / 2)*(2*i + 1), 3f, initialcoor.z + (rect_height / 2));
			rects[i].transform.position = worldpos;

			worldz[i]= worldpos.z;
		}
	}

	void screenBounds () {
		
		Vector3 screenPosfi = touchcamera.WorldToScreenPoint(touchcamera.transform.position);
		
		centralpixel = screenPosfi;
		
		screen_height = touchcamera.pixelHeight;
		screen_lenght = touchcamera.pixelWidth;
		
		initialcoor = touchcamera.ScreenToWorldPoint (new Vector3(centralpixel.x - screen_lenght / 2, centralpixel.y - screen_height / 2, 0f));
		finalcoor = touchcamera.ScreenToWorldPoint (new Vector3(centralpixel.x + screen_lenght / 2, centralpixel.y + screen_height / 2, 0f));
		
		plane_height = finalcoor.z - initialcoor.z;
		plane_lenght = finalcoor.x - initialcoor.x;

	}

	void Scale(){

		for (int i=0; i< worldz.Length; i++) {

			float cval = worldz[i] - z_min;

			scaledval[i] = cval / rect_height;
		}

	}

	void Transfer_func(){

		float div = 255f / 10f;

		for (int i = 0; i< scaledval.Length; i++) {

			int one = Mathf.RoundToInt(i*div);
			int two = Mathf.RoundToInt((i+1)*div);

			for(int j= one; j<= two; j++){
				trans_func[j] = scaledval[i];
			}
		}
		/*
		for (int i = 0; i<= 28; i++) {
			trans_func[i] = scaledval[0];
		}

		
		for (int i = 29; i<= 57; i++) {
			trans_func[i] = scaledval[1];
		}

		
		for (int i = 58; i<= 85; i++) {
			trans_func[i] = scaledval[2];
		}

		
		for (int i = 86; i<= 113; i++) {
			trans_func[i] = scaledval[3];
		}

		
		for (int i = 114; i<=141; i++) {
			trans_func[i] = scaledval[4];
		}

		
		for (int i = 142; i<= 169; i++) {
			trans_func[i] = scaledval[5];
		}

		
		for (int i = 170; i<= 197; i++) {
			trans_func[i] = scaledval[6];
		}

		
		for (int i = 198; i<= 225; i++) {
			trans_func[i] = scaledval[7];
		}

		
		for (int i = 226; i<= 255; i++) {
			trans_func[i] = scaledval[8];
		}
		*/
		customs.funcao_transferencia = trans_func;
	}

	void Adapt_1D (){

		//print("correu o adapt");
	//declaracao do vector que importei do cursor
		float[] transf_vector = customs.funcao_transferencia;
		/*
		float total;
		float nr;
		float mean;
		Vector3 pos;*/
		//Intervalos de intensidades


		float div = 255f / 10f;
		
		for (int i = 0; i< scaledval.Length; i++) {
			
			int one = Mathf.RoundToInt(i*div);
			int two = Mathf.RoundToInt((i+1)*div);

			float total = 0;
			float nr = 0;
			for(int j= one; j<= two; j++){
				if(transf_vector[j]!=0){
					total = total + transf_vector[j];
					nr = nr + 1f;
				}
			}

			float mean = total / nr;
			if (mean <= 1f && mean >= 0f) {
			} else {
				mean=0f;
			}
			Vector3 pos = rects [i].transform.position;
			pos.z = initialcoor.z - (rect_height / 2f) + (mean * rect_height);
			rects [i].transform.position = pos;
		}
		/*
		total = 0;
		nr = 0;
		for (int i = 0; i<= 28; i++) {
			if(transf_vector[i]!=0){
				total = total + transf_vector[i];
				nr = nr + 1f;
			}
		}
		mean = total / nr;
		if (mean <= 1f && mean >= 0f) {
		} else {
			mean=0f;
		}
		print (mean);
		pos = rects [0].transform.position;
		pos.z = initialcoor.z - (rect_height / 2f) + (mean * rect_height);
		rects [0].transform.position = pos;

		total = 0;
		nr = 0;
		for (int i = 29; i<= 57; i++) {
			if(transf_vector[i]!=0){
				total = total + transf_vector[i];
				nr = nr + 1f;
			}
		}
		mean = total / nr;
		if (mean <= 1f && mean >= 0f) {
		} else {
			mean=0f;
		}
		print (mean);
		pos = rects [1].transform.position;
		pos.z = initialcoor.z - (rect_height / 2f) + (mean * rect_height);
		rects [1].transform.position = pos;

		total = 0;
		nr = 0;
		for (int i = 58; i<= 85; i++) {
			if(transf_vector[i]!=0){
				total = total + transf_vector[i];
				nr = nr + 1f;
			}
		}
		mean = total / nr;
		if (mean <= 1f && mean >= 0f) {
		} else {
			mean=0f;
		}
		print (mean);
		pos = rects [2].transform.position;
		pos.z = initialcoor.z - (rect_height / 2f) + (mean * rect_height);
		rects [2].transform.position = pos;

		total = 0;
		nr = 0;
		for (int i = 86; i<= 113; i++) {
			if(transf_vector[i]!=0){
				total = total + transf_vector[i];
				nr = nr + 1f;
			}
		}
		mean = total / nr;
		if (mean <= 1f && mean >= 0f) {
		} else {
			mean=0f;
		}
		print (mean);
		pos = rects [3].transform.position;
		pos.z = initialcoor.z - (rect_height / 2f) + (mean * rect_height);
		rects [3].transform.position = pos;

		total = 0;
		nr = 0;
		for (int i = 114; i<= 141; i++) {
			if(transf_vector[i]!=0){
				total = total + transf_vector[i];
				nr = nr + 1f;
			}
		}
		mean = total / nr;
		if (mean <= 1f && mean >= 0f) {
		} else {
			mean=0f;
		}
		print (mean);
		pos = rects [4].transform.position;
		pos.z = initialcoor.z - (rect_height / 2f) + (mean * rect_height);
		rects [4].transform.position = pos;

		total = 0;
		nr = 0;
		for (int i = 142; i<= 169; i++) {
			if(transf_vector[i]!=0){
				total = total + transf_vector[i];
				nr = nr + 1f;
			}
		}
		mean = total / nr;
		if (mean <= 1f && mean >= 0f) {
		} else {
			mean=0f;
		}
		print (mean);
		pos = rects [5].transform.position;
		pos.z = initialcoor.z - (rect_height / 2f) + (mean * rect_height);
		rects [5].transform.position = pos;

		total = 0;
		nr = 0;
		for (int i = 170; i<= 197; i++) {
			if(transf_vector[i]!=0){
				total = total + transf_vector[i];
				nr = nr + 1f;
			}
		}
		mean = total / nr;
		if (mean <= 1f && mean >= 0f) {
		} else {
			mean=0f;
		}
		print (mean);
		pos = rects [6].transform.position;
		pos.z = initialcoor.z - (rect_height / 2f) + (mean * rect_height);
		rects [6].transform.position = pos;

		total = 0;
		nr = 0;
		for (int i = 198; i<= 225; i++) {
			if(transf_vector[i]!=0){
				total = total + transf_vector[i];
				nr = nr + 1f;
			}
		}
		mean = total / nr;
		if (mean <= 1f && mean >= 0f) {
		} else {
			mean=0f;
		}
		print (mean);
		pos = rects [7].transform.position;
		pos.z = initialcoor.z - (rect_height / 2f) + (mean * rect_height);
		rects [7].transform.position = pos;

		total = 0;
		nr = 0;
		for (int i = 226; i<= 255; i++) {
			if(transf_vector[i]!=0){
				total = total + transf_vector[i];
				nr = nr + 1f;
			}
		}
		mean = total / nr;
		if (mean <= 1f && mean >= 0f) {
		} else {
			mean=0f;
		}
		print (mean);
		pos = rects [8].transform.position;
		pos.z = initialcoor.z - (rect_height / 2f) + (mean * rect_height);
		rects [8].transform.position = pos;
		*/
	}

}
