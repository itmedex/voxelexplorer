﻿using UnityEngine;
using System.Collections;

public class RectControl : MonoBehaviour {

	Camera touchcamera;
	public Camera usecamera;
	private bool _mouseState;
	public GameObject Target;
	public Vector3 screenSpace;
	public Vector3 offset;

	// Posiçao de refrencia
	private bool set = true;
	private Vector3 worldpos;

	// Use this for initialization
	void Start ()
	{
		touchcamera = usecamera;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (set) {
			worldpos = transform.position;
			set = false;
		}
		
		// Debug.Log(_mouseState);
		if (Input.GetMouseButtonDown (0)) {
			RaycastHit hitInfo;
			if (Target == GetClickedObject (out hitInfo)) {
				_mouseState = true;
				screenSpace = touchcamera.WorldToScreenPoint (Target.transform.position);
				offset = Target.transform.position - touchcamera.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, screenSpace.z));
			}
		}
		if (Input.GetMouseButtonUp (0)) {
			_mouseState = false;
		}
		if (_mouseState) {
			//keep track of the mouse position
			var curScreenSpace = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, screenSpace.z);
			
			//convert the screen mouse position to world point and adjust with offset
			var curPosition = touchcamera.ScreenToWorldPoint (curScreenSpace) + offset;
			
			//update the position of the object in the world
			Target.transform.position = curPosition;
			CheckPos ();
		}
	}
	
	
	GameObject GetClickedObject (out RaycastHit hit)
	{
		GameObject target = null;
		Ray ray = touchcamera.ScreenPointToRay (Input.mousePosition);
		if (Physics.Raycast (ray.origin, ray.direction * 10, out hit)) {
			target = hit.collider.gameObject;
		}
		
		return target;
	}
	
	void CheckPos(){

		Vector3 newpos = transform.position;

		if (transform.position.x != worldpos.x) {
			newpos.x = worldpos.x;
		}

		
		if (transform.position.y != worldpos.y) {
			newpos.y = worldpos.y;
		}

		
		if (transform.position.z > worldpos.z) {
			newpos.z = worldpos.z;
		}

		if (transform.position.z < worldpos.z - transform.localScale.z) {
			newpos.z = worldpos.z - transform.localScale.z;
		}

		transform.position = newpos;
		
	}
}
