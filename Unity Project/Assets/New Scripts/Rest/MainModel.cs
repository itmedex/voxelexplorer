﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class MainModel : MonoBehaviour {

	// Vectores de suporte para o modelo
	// Vector com cores originais
	Color[,,] cores;
	// Vector de sistema de particulas inalterado
	private ParticleSystem.Particle[] original_points;
	// Vector de sistema de particulas alterado
	private ParticleSystem.Particle[] current_points;
	// Coordenadas originais de cada ponto
	private Vector3[] original_point;

	//Chama as figuras
	public int fatias;
	string[] slices;  

	// Medidas da figura
	Texture2D figura;
	public int altura = 0;
	public int largura = 0;

	// Vector do histograma
	public float[] histograma;

	// Use this for initialization
	void Start () {

		// Cria um vector com o nome das imagens a fazer load
		slices = new string[fatias];
		for (int nr = 2; nr < fatias + 2; nr++) {
			string myString = nr.ToString ();
			slices [nr - 2] = myString;
		}

		// Obtem as medidas das imagens
		
		figura = (Texture2D)Resources.Load(slices[0]);
		altura = figura.height;
		largura = figura.width;
		cores = new Color[largura,altura,fatias];
		
		// Cria o vector cores
		int i = 0;
		for (int z = 0; z < fatias; z++) {
			figura = (Texture2D)Resources.Load (slices[z]);
			for (int x = 0; x < largura; x++) {
				for (int y = 0; y < altura; y++) {
					cores [x,y,z] = figura.GetPixel (x, y);
					i++;
				}
			}
		}

		// Criar o Vector do histograma
		histograma = new float[256];
		
		for (int z = 0; z < fatias; z++) {
			for (int x = 0; x < largura; x++) {
				for (int y = 0; y < altura; y++) {
					int intval = Mathf.RoundToInt(GetIntens(x,y,z)*255);
					histograma[intval] = histograma[intval] + 1f;
				}
			}
		}




	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	float GetIntens ( int x, int y, int z){
		
		// determinar intesidade
		Color co = cores [x, y, z];
		
		float co_intens = ((co.r + co.b + co.g) / 3);
		
		return co_intens;
	}
}
