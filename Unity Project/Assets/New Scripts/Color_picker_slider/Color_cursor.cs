﻿using UnityEngine;
using System.Collections;

public class Color_cursor : MonoBehaviour {

	Camera touchcamera;
	public Camera usecamera;
	private bool _mouseState;
	public GameObject Target;
	public Vector3 screenSpace;
	public Vector3 offset;

	// variavel touch
	private Touch touch;
	private float tap_timer;
	private int tap_count;

	// Objectos da janela
	public GameObject background;

	private GameObject color_picker;

	private Camera color_camera;

	private Function_draw drawing;
	private Color_Generator function;

	float check_time;
	int touches;
	public GameObject grapher;

	private New_Movement_controls screen;

	// Variaveis de interesse
	public Color selected_color;
	public bool change;
	

	// Use this for initialization
	void Start ()
	{
		GameObject cam = GameObject.FindGameObjectWithTag ("color_picker_camera");
		Camera camera = cam.GetComponent<Camera> ();
		touchcamera = camera;

		GameObject back = GameObject.FindGameObjectWithTag ("color_picker_background");
		background = back;

		GameObject touch_camera = GameObject.FindGameObjectWithTag("touch_camera");
		Function_draw draw = touch_camera.GetComponent<Function_draw>();
		drawing = draw;

		GameObject colorgenerator = GameObject.FindGameObjectWithTag ("color_picker_camera");
		function = colorgenerator.GetComponent<Color_Generator>();

		GameObject mainscreen = GameObject.FindGameObjectWithTag ("MainCamera");
		screen = mainscreen.GetComponent<New_Movement_controls>();
	
		color_picker = GameObject.FindGameObjectWithTag ("color_picker");

		GameObject ccscreen = GameObject.FindGameObjectWithTag ("color_camera");
		color_camera = ccscreen.GetComponent<Camera> ();

		//getcolor();
		//function.SendMessage("Update_color", SendMessageOptions.DontRequireReceiver);

		Select ();
	}
	
	// Update is called once per frame
	void Update ()
	{
	
		if (change) {
			screen.unlocked = false;
			drawing.unlocked = false;
			// mudar a posiçao do color picker
			Set_picker ();
			getcolor ();
			function.SendMessage ("Update_color", SendMessageOptions.DontRequireReceiver);
		} 

		selected_color = GetComponent<Renderer>().material.color;


		tap_count = 0;

		// Debug.Log(_mouseState);
		if (TuioInput.touchCount > 0) {
			touch = TuioInput.touches [0];
		}



		if (touch.phase == TouchPhase.Began) {
			RaycastHit hitInfo;
			if (Target == GetClickedObject (out hitInfo)) {
				_mouseState = true;
				//drawing.unlocked = false;
				//screen.unlocked = false;
				tap_timer = Time.time + 1f;

				/*
				// Ve toques e instancia um novo cursor
				if (Time.time > check_time + 0.5f){
					touches = 1;
					check_time = Time.time;
				}else{
					touches = touches + 1;
					check_time = Time.time;
				}


				if (touches == 2){
					if(change){
						Deselect();
					}else{
						Select();
					}
				}
				*/
				screenSpace = touchcamera.WorldToScreenPoint (Target.transform.position);
				offset = Target.transform.position - touchcamera.ScreenToWorldPoint (new Vector3 (touch.position.x, touch.position.y, screenSpace.z));
			}


		}
		if (touch.phase == TouchPhase.Ended) {
			if(_mouseState){
				if(!Check_interval()) Check_state();
				function.SendMessage("Update_color", SendMessageOptions.DontRequireReceiver);
				drawing.unlocked = true;
				screen.unlocked = true;


				if(Time.time < tap_timer) {
					tap_count = 1;
				}
				
				if (tap_count == 1){
					if(change){
						Deselect();
						//screen.unlocked = true;
					}else{
						Select();
					}
				}

			}
			_mouseState = false;

			/*
			if(Time.time < tap_timer) {
				tap_count = 1;
			}

			if (tap_count == 1){
				if(change){
					Deselect();
					//screen.unlocked = true;
				}else{
					Select();
				}
			}*/
		}

		if (_mouseState) {
			function.SendMessage("Update_color", SendMessageOptions.DontRequireReceiver);
			screen.unlocked = false;
			//keep track of the mouse position
			var curScreenSpace = new Vector3 (touch.position.x, touch.position.y, screenSpace.z);
			
			//convert the screen mouse position to world point and adjust with offset
			var curPosition = touchcamera.ScreenToWorldPoint (curScreenSpace) + offset;
			
			//update the position of the object in the world
			Target.transform.position = curPosition;
		}
	}
	
	
	GameObject GetClickedObject (out RaycastHit hit)
	{
		GameObject target = null;
		Ray ray = touchcamera.ScreenPointToRay (touch.position);
		if (Physics.Raycast (ray.origin, ray.direction * 10, out hit)) {
			target = hit.collider.gameObject;
		}
		
		return target;
	}

	private bool Check_interval(){

		Vector2 screen_point = touchcamera.WorldToScreenPoint (transform.position);

		if (screen_point.x > drawing.left && screen_point.x < drawing.right && screen_point.y > drawing.bottom && screen_point.y < drawing.top) {
			//print ("mandou mensagem para os intervalos");
			Deselect();
			background.SendMessage ("Interval_color", this.gameObject, SendMessageOptions.DontRequireReceiver);
			return true;
		} else {
			return false;
		}

	}

	public void Check_state(){

		Vector3 back_pos = background.transform.position;

		float up = back_pos.z + (background.transform.localScale.z/2);
		float down = back_pos.z - (background.transform.localScale.z/2);
		float left = back_pos.x - (background.transform.localScale.x / 2);
		float right = back_pos.x + (background.transform.localScale.x / 2);

		if (transform.position.x > right || transform.position.x < left || transform.position.z > up || transform.position.z < down) {
			//print ("destroy");
			Deselect();
			GameObject.Destroy(gameObject);
		} else {
			Vector3 new_pos = transform.position;
			new_pos.z = background.transform.position.z;
			transform.position = new_pos;
		}

	}


	void getcolor(){

		GameObject square_color = GameObject.FindGameObjectWithTag ("color_indicator");
		Material colorsquare = square_color.GetComponent<Renderer>().material;

		//Criaçao da textura e atribuiçao de cor obtida pelo quadrado
	
		gameObject.GetComponent<Renderer>().material = colorsquare;
		selected_color = colorsquare.color;
	}

	void Deselect(){

		change = false;
		color_picker.transform.position = new Vector3 ( -100f, -311f, -400f);
	}

	public void Select(){

		Color c_var = GetComponent<Renderer>().material.color;
		color_picker.SendMessage ("Set_cursors", c_var);

		GameObject[] cursores = GameObject.FindGameObjectsWithTag("color_cursor");

		foreach (GameObject cursor in cursores) {
			cursor.SendMessage ("Deselect", SendMessageOptions.DontRequireReceiver);
		}

		change = true;


	}

	void Set_picker(){

		Vector3 screen_coor = touchcamera.WorldToScreenPoint (transform.position);

		float height = touchcamera.GetComponent<Camera>().pixelHeight;
		float width = touchcamera.GetComponent<Camera>().pixelWidth;

		Vector3 new_screen_coor = new Vector3 ((width * 0.04f) + screen_coor.x, (height * -0.1f) + screen_coor.y, 32f);

		color_picker.transform.position = color_camera.ScreenToWorldPoint (new_screen_coor);

	}

}
