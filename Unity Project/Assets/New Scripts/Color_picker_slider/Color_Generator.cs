﻿using UnityEngine;
using System.Collections;

public class Color_Generator : MonoBehaviour {


	public GameObject background;
	public Background back;
	public Grapher5Up grapher;

	private int nr_cursor;
	private int last_number;
	public int[] defined;
	public Color[] colors;

	//Vector para a aplicaçao da cor
	public float[] applycolor;
	

	//
	Vector3 back_pos;
	float left;
	float right;

	float check_time;
	int touches;

	bool first = true;


	void Start () {
	
		nr_cursor = 0;
		last_number = 0;
		defined = new int[256];
		colors = new Color[256];

		back_pos = background.transform.position;
		left = back_pos.x - (background.transform.localScale.x / 2);
		right = back_pos.x + (background.transform.localScale.x / 2);
	}
	

	void FixedUpdate () {

		if (first) {
			left = back_pos.x - (background.transform.localScale.x / 2);
			right = back_pos.x + (background.transform.localScale.x / 2);
			first = false;
		}


		GameObject[] cursores = GameObject.FindGameObjectsWithTag("color_cursor");
		nr_cursor = cursores.Length;

		if (nr_cursor != last_number) {
			//print ("updated color");
			Update_color();
			last_number = nr_cursor;
		}


	}

	void Update_color(){

		defined = new int[256];
		colors = new Color[256];

		GameObject[] cursores = GameObject.FindGameObjectsWithTag("color_cursor");


		foreach (GameObject cursor in cursores) {

			Color_cursor valores = cursor.GetComponent<Color_cursor>();

			float pos = cursor.transform.position.x;

			int posint = Mathf.RoundToInt(((pos-left)/(right-left))*255);


				defined[posint]= 1;
				colors[posint] = valores.selected_color;


		}

		if (defined [0] == 0) {
			defined[0]=1;
			colors[0]= Color.black;
		}

		if (defined [255] == 0) {
			defined[255]=1;
			colors[255]= Color.white;
		}

		bool begin = false;

		for (int i=0; i < defined.Length; i++) {

			if (defined[i]==1 ){
				begin = true;
			}

			if (defined[i] == 0 && begin){

				// Encontrar proximo ponto com cor
				int j = i + 1;

				while(j < defined.Length && defined[j]==0){
					j++;
				}

				if(j<256){
					//print ("lerped");
					// Interpolar linearmente as cores entre os pontos
					float fj = j;
					float fi = i;
					float t = 1/(fj-fi+1f);
					defined[i] = 1;
					colors[i] = Color.Lerp(colors[i-1], colors[j],t);
				}
			}

		}
		//print ("colors updated " + cursores.Length);
		grapher.SendMessage ("Apply_color", SendMessageOptions.DontRequireReceiver);
		back.SendMessage ("Apply_color", SendMessageOptions.DontRequireReceiver);
	}

	}