﻿using UnityEngine;
using System.Collections;

public class color_l : MonoBehaviour {

	Camera touchcamera;
	public Camera usecamera;
	private bool _mouseState;
	public GameObject Target;
	public Vector3 screenSpace;
	public Vector3 offset;

	// variavel touch
	private Touch touch;
	
	// Objecto 
	public GameObject Right;
	public GameObject window;

	private Function_draw drawing;
	private Color_Generator function;

	private Manipulateobject screen;

	void Start ()
	{
		GameObject cam = GameObject.FindGameObjectWithTag ("color_picker_camera");
		Camera camera = cam.GetComponent<Camera> ();
		touchcamera = camera;
		GameObject touch_camera = GameObject.FindGameObjectWithTag("touch_camera");
		Function_draw draw = touch_camera.GetComponent<Function_draw>();
		drawing = draw;
		GameObject colorgenerator = GameObject.FindGameObjectWithTag ("color_picker_camera");
		function = colorgenerator.GetComponent<Color_Generator>();
		GameObject mainscreen = GameObject.FindGameObjectWithTag ("MainCamera");
		screen = mainscreen.GetComponent<Manipulateobject>();
	}

	void Update ()
	{
		CheckPos ();
		
		// Debug.Log(_mouseState);
		if (TuioInput.touchCount > 0) {
			touch = TuioInput.touches [0];
		}
		
		if (touch.phase == TouchPhase.Began) {
			RaycastHit hitInfo;
			if (Target == GetClickedObject (out hitInfo)) {
				_mouseState = true;
				drawing.unlocked = false;
				screen.unlocked = false;
				screenSpace = touchcamera.WorldToScreenPoint (Target.transform.position);
				offset = Target.transform.position - touchcamera.ScreenToWorldPoint (new Vector3 (touch.position.x, touch.position.y, screenSpace.z));
			}
		}
		if (touch.phase == TouchPhase.Ended) {
			if(_mouseState){
				function.SendMessage("Update_color", SendMessageOptions.DontRequireReceiver);
				drawing.unlocked = true;
				screen.unlocked = true;
			}
			_mouseState = false;

		}
		if (_mouseState) {
			//keep track of the mouse position
			var curScreenSpace = new Vector3 (touch.position.x, touch.position.y, screenSpace.z);
			
			//convert the screen mouse position to world point and adjust with offset
			var curPosition = touchcamera.ScreenToWorldPoint (curScreenSpace) + offset;
			
			//update the position of the object in the world
			Target.transform.position = curPosition;

			CheckPos2();
			

		}
	}

	GameObject GetClickedObject (out RaycastHit hit)
	{
		GameObject target = null;
		Ray ray = touchcamera.ScreenPointToRay (touch.position);
		if (Physics.Raycast (ray.origin, ray.direction * 10, out hit)) {
			target = hit.collider.gameObject;
		}
		
		return target;
	}

	void CheckPos(){
		
		Vector3 newpos = transform.position;
		
		if (transform.position.x > Right.transform.position.x) {
			newpos.x = Right.transform.position.x;
		}
		if (transform.position.z != window.transform.position.z) {
			newpos.z = window.transform.position.z;
		}
		if (transform.position.x != window.transform.position.x - window.transform.localScale.x / 2) {
			newpos.x = window.transform.position.x - window.transform.localScale.x / 2;
		}
		if (newpos != transform.position) {
			transform.position = newpos;
		}
	}

	void CheckPos2(){
		Vector3 newpos = transform.position;

		if(transform.position.z != window.transform.position.z) {
			newpos.z = window.transform.position.z;

		}
		if (newpos != transform.position) {
			transform.position = newpos;
		}
	}
}
