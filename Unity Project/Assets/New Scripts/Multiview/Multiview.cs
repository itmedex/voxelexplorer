﻿using UnityEngine;
using System.Collections;

public class Multiview : MonoBehaviour {

	// Cameras
	public GameObject camera_1;
	public GameObject camera_2;
	public GameObject camera_3;
	public GameObject camera_4;
	public GameObject camera_5;
	public GameObject camera_6;
	public GameObject camera_7;
	public GameObject camera_8;
	public GameObject camera_9;
	public GameObject camera_10;

	//Vector de camaras
	private GameObject[] cameras;

	// Modelo a instanciar em frente a cada objecto
	private GameObject model;

	// Origem dos vectores do sistema de particulas
	public Grapher5Up main;

	// Sistema de particulas
	private ParticleSystem.Particle[] original;
	private ParticleSystem.Particle[] apply;
	private Color[,,] cores;
	private Vector3[] pos_original;
	private float[] histograma;

	// Use this for initialization
	void Start () {

		cameras = new GameObject[10];

		cameras [0] = camera_1;
		cameras [1] = camera_2;
		cameras [2] = camera_3;
		cameras [3] = camera_4;
		cameras [4] = camera_5;
		cameras [5] = camera_6;
		cameras [6] = camera_7;
		cameras [7] = camera_8;
		cameras [8] = camera_9;
		cameras [9] = camera_10;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void Create_views(){

		original = main.thumb_points;

		// Criaçao dos modelos
		// Arranjar forma de criar 9 sistemas, cada um com apenas os pontos de interesse


		// Instanciar e posicionar o modelo
		for (int nr = 0; nr < cameras.Length; nr++) {

			Vector3 worldpos = cameras[nr].transform.position;
			worldpos.x = worldpos.x;
			worldpos.y = worldpos.y - 1f;
			worldpos.z = worldpos.z; 

			model = (GameObject)Instantiate (Resources.Load ("Multigraph"));

			model.transform.position = worldpos;

			apply = original;

			// definir o intervalo
			float intervalo = 255f / cameras.Length;
			int pontoi = Mathf.RoundToInt(nr*intervalo);
			int pontof = Mathf.RoundToInt((nr+1)*intervalo);

		
			// definir as particulas
			for (int i = 0; i < original.Length; i++) {
				// Vai buscar intensidade original
				/*Vector3 po = pos_original[i];
				int x = Mathf.RoundToInt(po.x);
				int y = Mathf.RoundToInt(po.y);
				int z = Mathf.RoundToInt(po.z);
				Color c = cores[x, y, z];*/
				// Cor do modelo
				Color c_m = apply [i].color;
				//int gray_value = Mathf.FloorToInt(255*(c_m.r + c_m.g + c_m.b)/3);
				int gray_value = Mathf.FloorToInt(255f * c_m.grayscale);

				if(gray_value < pontoi || gray_value > pontof){
					c_m.a = 0f;
				}
				if(gray_value >= pontoi && gray_value <= pontof){
					c_m.a = 1f;
				}
				if(gray_value == 0) c_m.a = 0f;



				apply [i].color = c_m;
			}


			model.GetComponent<ParticleSystem>().SetParticles (apply, original.Length);
		}
	}
}
