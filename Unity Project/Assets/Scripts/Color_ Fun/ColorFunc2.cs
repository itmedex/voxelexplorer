﻿using UnityEngine;
using System.Collections;

public class ColorFunc2 : MonoBehaviour {


	// Pontos de referencia
	// Sao definidas as cores para a intensidade maxima e minima
	private Color low_color = Color.black;
	private Color high_color = Color.white;

	// Limites dos valores dos tecidos
	// Lung
	private int lung_low = 26;
	private int lung_high = 38;
	
	// Fat
	private int fat_low = 57;
	private int fat_high = 61;
	
	// Water
	private int water_low = 64;
	private int water_high = 64;

	// Kidney
	private int kidney_low = 65;
	private int kidney_high = 66;

	// Blood
	private int blood_low = 66;
	private int blood_high = 66;

	// Muscle
	private int muscle_low = 64;
	private int muscle_high = 66;

	// Grey Matter
	private int greym_low = 66;
	private int greym_high = 67;

	// White Matter
	private int whitem_low = 65;
	private int whitem_high = 65;

	// Liver
	private int liver_low = 66;
	private int liver_high = 68;

	// Bone
	private int bone_low = 128;
	private int bone_high = 255;

	
	// Vector de Intervalos
	private Vector2[] intervalos;

	public SelectColor SC;

	// Vector de Cores seleccionadas - Comp. 10, associa um tecido a uma cor
	private Color[] Cores_selec;

	// Vector de cores dos tecidos - Comp. 256, associa uma cor a uma intensidade
	private Color[] Tissue_color;

	// Vector de Selecçao de tecidos - Comp.10, cada posiçao e um tecido
	private int[] selected_tissue;

	// Vector de selecçao das intensidades dos tecidos - Comp.256, atribui 1 aos valores de intensidade dos tecidos activados 
	private int[] selected_val;

	// Vector a exportar com as cores
	public Color[] Color_set;

	void Start(){

		intervalos = new Vector2[10];
		intervalos [0].x = lung_low;
		intervalos [0].y = lung_high;
		intervalos [1].x = fat_low;
		intervalos [1].y = fat_high;
		intervalos [2].x = water_low;
		intervalos [2].y = water_high;
		intervalos [3].x = kidney_low;
		intervalos [3].y = kidney_high;
		intervalos [4].x = blood_low;
		intervalos [4].y = blood_high;
		intervalos [5].x = muscle_low;
		intervalos [5].y = muscle_high;
		intervalos [6].x = greym_low;
		intervalos [6].y = greym_high;
		intervalos [7].x = whitem_low;
		intervalos [7].y = whitem_high;
		intervalos [8].x = liver_low;
		intervalos [8].y = liver_high;
		intervalos [9].x = bone_low;
		intervalos [9].y = bone_high;

		Reset_Grey ();
	}

	void Link_Color(){


		Cores_selec = SC.cores_tecidos;
		selected_tissue = SC.tecidos_activados;

		Tissue_color = new Color[256];
		selected_val = new int[256];

		// Identifica a cor e a intensidade dos tecidos seleccionados
		for (int i = 0; i < intervalos.Length; i++) {
			if (selected_tissue [i] == 1) {
				int x = Mathf.RoundToInt(intervalos[i].x);
				int y = Mathf.RoundToInt(intervalos[i].y);
				for (int j = x; j < y; j++) {
					Tissue_color[j] = Cores_selec[i];
					selected_val[j] = 1;
				}
			}
		}

		// Interpolar as cores nos lugares vazios
		// Definir os limites
		selected_val [0] = 1;
		Tissue_color [0] = low_color;
		selected_val [255] = 1;
		Tissue_color [255] = high_color;

		for (int i = 1; i < Tissue_color.Length; i++) {

			if (selected_val[i]==0){
				// Encontrar proximo ponto com cor
				int j = i + 1;
				while(selected_val[j]==0){
					j++;
				}
				// Interpolar linearmente as cores entre os pontos
				float t = 1/(j-i+1);
				Tissue_color[i] = Color.Lerp(Tissue_color[i-1], Tissue_color[j],t);
			}
		}
		Color_set = Tissue_color;
	}

	// Funçao para fazer reset da imagem para preto e branco. Se calhar nao e necessario.
	public void Reset_Grey(){

		Tissue_color = new Color[256];
		selected_val = new int[256];

		selected_val [0] = 1;
		Tissue_color [0] = low_color;
		selected_val [255] = 1;
		Tissue_color [255] = high_color;

		for (int i = 1; i < Tissue_color.Length; i++) {
			float t = i/(255);
			Tissue_color[i] = Color.Lerp(Tissue_color[0], Tissue_color[255],t);
			/*
			Color c;
			c = Color.white;
			c.r = t;
			c.g = t;
			c.b = t;
			Tissue_color[i] = c;
			*/
			}
		Color_set = Tissue_color;
	}

} 

