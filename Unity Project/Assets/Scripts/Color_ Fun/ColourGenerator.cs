﻿using UnityEngine;
using System.Collections;

public class ColourGenerator : MonoBehaviour {
	
	// Tecidos a Utilizar
	public bool bone;
	public bool muscle;

	// Pontos de referencia
	// Sao criados dois pontos, um em cada estremo, como referencia
	private float low_isoval = 0;
	private float high_isoval = 255;
	private Color low_color = Color.black;
	private Color high_color = Color.white;

	// Ossos

	[Range(0,255)]
	public float bone_isoval;
	public Color bone_color;

	// Musculo

	[Range(0,255)]
	public float muscle_isoval;
	public Color muscle_color;

	// Vector de isovalores
	private float[] iso_vect;

	// Vectores de cores
	private float[] red_vect;
	private float[] green_vect;
	private float[] blue_vect;

	// Spline vector dos isovalores
	private float[] spline_isoval;

	// Spline vector das cores
	private float[] spline_red;
	private float[] spline_green;
	private float[] spline_blue;

	// Script do Spline

	// Spline Red
	private Vector3[] red_values;
	private Vector3[] red_values_1;
	private float[] red_values_2;
	public int inc;

	// Spline Green
	private float[] green_values;

	// Spline Blue
	private float[] blue_values;

	// Funçao de cores a ser exportada

	public Vector3[] color_transfer;


	void Start(){

		SampleVectors ();

		IsoSpline ();

		// Preparar os vectores para aplicar a funçao de interpolaçao

		// Preparar red
		/*
		red_values = new Vector3[4];
		for (int i=0; i<4; i++) {
			red_values[i].x = iso_vect[i];
			red_values[i].y = red_vect[i];
		}
		*/
		// Mostrar vector original
		/*for (int i=0; i < 4; i++) {
			print ("em " + red_values [i].x + " tens " + red_values [i].y + " e " + red_values [i].z);
		}
		*/
		// Primeiro Script
		/*float smoothness = 64f;
		red_values_1 = Curver.MakeSmoothCurve (red_values, smoothness);

		print ("length of first interpolation is "+red_values_1.Length);

		for (int i=0; i < 256; i = i + inc) {
			print ("em " + red_values_1 [i].x + " tens " + red_values_1 [i].y + " e " + red_values_1 [i].z);
		}
		*/
		// Segundo script

		red_values_2 = CubicSpline (iso_vect, red_vect, 256);

		for (int i=0; i < red_values_2.Length; i++) {
			if(red_values_2[i]<0){
				red_values_2[i] = 0;
			}
		}

		/*
		print ("length of second interpolation is "+red_values_2.Length);
		
		for (int i=0; i < 256; i = i + inc) {
			print ("em " + i + " tens " + red_values_2 [i]);
		}
		*/

		green_values = CubicSpline (iso_vect, green_vect, 256);
		
		for (int i=0; i < green_values.Length; i++) {
			if(green_values[i]<0){
				green_values[i] = 0;
			}
		}

		blue_values = CubicSpline (iso_vect, blue_vect, 256);
		
		for (int i=0; i < blue_values.Length; i++) {
			if(blue_values[i]<0){
				blue_values[i] = 0;
			}
		}

		// Criar o vector a exportar

		color_transfer = new Vector3[256];

		for (int i=0; i<256; i++) {
			color_transfer[i].x = red_values_2[i];
			color_transfer[i].y = green_values[i];
			color_transfer[i].z = blue_values[i];
		}
	}

	void SampleVectors() {

		// Preencher os vectores de isovalores 

		iso_vect = new float[4];

		iso_vect [0] = low_isoval;
		iso_vect [1] = muscle_isoval;
		iso_vect [2] = bone_isoval;
		iso_vect [3] = high_isoval;

		// Preencher os vectores de cores

		// Red
		red_vect = new float[4];

		red_vect [0] = low_color.r;
		red_vect [1] = muscle_color.r;
		red_vect [2] = bone_color.r;
		red_vect [3] = high_color.r;

		// Green
		green_vect = new float[4];

		green_vect [0] = low_color.g;
		green_vect [1] = muscle_color.g;
		green_vect [2] = bone_color.g;
		green_vect [3] = high_color.g;

		// Blue
		blue_vect = new float[4];

		blue_vect [0] = low_color.b;
		blue_vect [1] = muscle_color.b;
		blue_vect [2] = bone_color.b;
		blue_vect [3] = high_color.b;


	}

	void IsoSpline() {

		spline_isoval = new float[256];

		for(int i=0; i < spline_isoval.Length; i++){
			spline_isoval[i]=i;
		}
	}

	public float[] CubicSpline(float[] x, float[] y, int xi_length)
	{
		int n = x.Length;
		float[] h = new float[n];
		float[] b = new float[n];
		for (int i = 0; i < n-1; i++)
		{
			h[i] = x[i + 1] - x[i];
			b[i] = (y[i + 1] - y[i]) / h[i];
		}
		
		float[] u = new float[n];
		float[] v = new float[n];
		u[1] = 2*(h[0] + h[1]);
		v[1] = 6*(b[1] - b[0]);
		for (int i = 2; i < n-1; i++)
		{
			u[i] = 2*(h[i-1] + h[i]) - (h[i-1]*h[i-1])/u[i-1];
			v[i] = 6*(b[i] - b[i-1]) - (h[i-1]*v[i-1])/u[i-1];
		}
		
		float[] z = new float[n];
		z[n-1] = 0;
		for (int i = n-2; i > 0; i--)
			z[i] = (v[i] - h[i]*z[i+1]) / u[i];
		z[0] = 0;
		
		float[] S = new float[xi_length];
		int j = 0;
		for (int i = 0; i < S.Length; i++)
		{
			if (i >= x[j+1] && j < x.Length - 2)
				j++;
			float va = y[j];
			float vb = -(h[j]/6)*z[j+1] - (h[j]/3)*z[j] + (y[j+1]-y[j])/h[j];
			float vc = z[j]/2;
			float vd = (z[j+1]-z[j])/(6*h[j]);
			S[i] = va + (i-x[j])*(vb+(i-x[j])*(vc+(i-x[j])*vd));

			/*
			if(S[i]<0){
				S[i]=0f;
			}*/
		}
		
		return S;
	}

}
