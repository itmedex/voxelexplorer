﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class Observe : MonoBehaviour {


	public GameObject target;
	public bool moving = true;
	public float speed = 50f;
	private Vector3 defaultView;


	void Start () {
		moving = true;
		speed = 50f;
		transform.LookAt (target.transform);
		defaultView = transform.position;

	}

	void FixedUpdate (){
		if (moving) {
			RotateCamera ();
		}
	}

	void Update () {
		transform.LookAt (target.transform);
		if (Input.GetKey (KeyCode.R)) {
			ResetCamera ();
		}
	}

	void RotateCamera() {
			if (Input.GetKey (KeyCode.LeftArrow)) {
				transform.RotateAround (target.transform.position, Vector3.up , Time.deltaTime * speed);
			}
			if (Input.GetKey (KeyCode.RightArrow)) {
				transform.RotateAround (target.transform.position, Vector3.down , Time.deltaTime * speed);
			}
			if (Input.GetKey (KeyCode.DownArrow)) {
				transform.RotateAround (target.transform.position, Vector3.left , Time.deltaTime * speed);
			}
			if (Input.GetKey (KeyCode.UpArrow)) {
				transform.RotateAround (target.transform.position, Vector3.right , Time.deltaTime * speed);
			}
	}


	public void ResetCamera () {
			transform.position = defaultView;
	}
}
