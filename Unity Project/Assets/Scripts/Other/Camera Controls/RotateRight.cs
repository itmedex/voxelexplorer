﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class RotateRight : MonoBehaviour {

	bool buttonHeld = false;
	public GameObject target;
	public bool moving = true;
	public float speed = 50f;
	
	public void pressed (BaseEventData eventData)
	{
		buttonHeld = true;
	}
	
	public void notpressed(BaseEventData eventData)
	{
		buttonHeld = false;
	}
	
	void Start () {
		moving = true;
		speed = 50f;
		transform.LookAt (target.transform);
	}
	
	void Update () {
		if (moving) {
			if (buttonHeld) {
				RotateCameraRight ();
			}
		}
		
	}
	
	void RotateCameraRight () {
		transform.RotateAround (target.transform.position, Vector3.down , Time.deltaTime * speed);
	}

	public void RotateCameraRight (float swipe) {
		transform.RotateAround (target.transform.position, Vector3.down , Time.deltaTime * speed * swipe);
	}
}
