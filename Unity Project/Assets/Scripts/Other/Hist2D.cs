﻿using UnityEngine;
using System.Collections;

public class Hist2D : MonoBehaviour {

	public int resolution = 10;
	//private int currentResolution;

	// Recebe histograma
	public Histogram histo;
	private float[,]histograma;
	
	private ParticleSystem.Particle[] points;

	public void CreatePoints2D () {

		print ("sistema de particulas");

		if (resolution < 10 || resolution > 100) {
			Debug.LogWarning("Grapher resolution out of bounds, resetting to minimum.", this);
			resolution = 10;
		}

		histograma = histo.hist2d_final;

		//currentResolution = resolution;
		points = new ParticleSystem.Particle[resolution * resolution];

		float increment = 1f / (resolution - 1);
		int i = 0;
		for (int x = 0; x < resolution; x++) {
			for (int z = 0; z < resolution; z++) {
				Vector3 p = new Vector3(x * increment, 0f, z * increment);
				points[i].position = p;

				int x_pos = Mathf.RoundToInt(p.x * 255);
				int y_pos = Mathf.RoundToInt(p.y * 100);


				float intens = histograma[x_pos,y_pos];
				//print (intens);
				points[i].color = new Color(1f-intens, 1f-intens, 1f-intens);
				points[i++].size = 0.05f;
			}
		}

		GetComponent<ParticleSystem>().SetParticles(points, points.Length);
	}

}
