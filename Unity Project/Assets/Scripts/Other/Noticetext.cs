﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Noticetext : MonoBehaviour {

	Text instruction;
	private bool locked;
	
	void Start () {
		instruction = GetComponent<Text>();
		instruction.text = " ";
	}

	public void Toggle_text(){

		if (locked) {
			instruction.text = " ";
		} else {
			instruction.text = " Ready for input ";
		}

		locked = !locked;
	}
}
