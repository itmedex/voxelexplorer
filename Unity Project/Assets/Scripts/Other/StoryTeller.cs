﻿using UnityEngine;
using System.Collections;

public class StoryTeller : MonoBehaviour {
	
	// Verifica se insere ou nao o objecto
	private bool insert;

	// Objecto a inserir
	private GameObject story_cube;

	// Delay
	public float time_delay = 1f;
	private float time_act;

	// Distancia do ecra
	public float distance;

	// Use this for initialization
	void Start () {
		insert = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (insert) {
			if (Input.GetMouseButton(0) && Time.time > time_act) {

				story_cube = (GameObject)Instantiate(Resources.Load ("DecimalCube"));

				Vector3 worldPos = GetComponent<Camera>().ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0f));
				story_cube.transform.position = worldPos;

				story_cube.transform.Translate(Vector3.forward * distance);
				insert = !insert;
				print ("cube set");
			}
		}
	}

	public void Ready_cube(){
		insert = !insert;
		time_act = Time.time + time_delay;
		print ("cube ready");
	}
}
