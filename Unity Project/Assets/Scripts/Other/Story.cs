﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Story : MonoBehaviour {

	// Onde escrever

	private GameObject caixa;
	private Text display_text;

	// Texto a escrever
	private string texto;


	void Start () {
	
		texto = "";

		caixa = GameObject.FindGameObjectWithTag ("storydisp");
		display_text = caixa.GetComponent<Text> ();

	}
	

	void Update () {
	
	}

	public void Write_text (){
		
		texto = "Este e o texto do cubo que foi carregado";
		display_text.text = texto;
	}
}
