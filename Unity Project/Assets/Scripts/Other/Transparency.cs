﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Transparency : MonoBehaviour {

	public Slider alpha;
	private Color oC;
	private float redc;
	private float greenc;
	private float bluec;

	void Start (){
		GetValues ();
	}

	void Update () {
		ChangeAlpha ();
	}

	void GetValues (){
		Color oC = GetComponent<Renderer>().material.GetColor("_Color");
		redc = oC.r;
		greenc = oC.g;
		bluec = oC.b;
	}

	void ChangeAlpha () {
		GetComponent<Renderer>().material.color = new Color (redc, greenc, bluec, alpha.value);
	}
}