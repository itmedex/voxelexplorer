﻿using UnityEngine;
using System.Collections;

public class CustomFunction : MonoBehaviour {

	// Classe onde vai buscar a funçao incompleta
	public Cursor fonte;

	// Funçao incompleta
	private float[] inc_trans;

	// Funçao completa
	public float[] funcao_transferencia;

	// Intervalo a analisar
	public int nr = 20;

	// Classe que aplica a funçao
	public Grapher5Up sistema_part;

	// thresholds
	public float max;
	public float min;


	void Start(){

		funcao_transferencia = new float[256];
	}

	void Fullfunction(){

		inc_trans = fonte.transf;
		
		int low = 255;
		int high = 0;
		
		for (int i=0; i<inc_trans.Length; i++) {
			
			if (inc_trans[i] >= 0f && i <= low) {
				low = i;
			}
			
			if (inc_trans[i] >= 0f && i > high) high = i;
		}

		// Aproxima os valores da funçao de transferencia a multiplos de 0.1 (0.1, 0.2, 0.3, etc...)
		//Aproximate ();

		for (int x=1; x < inc_trans.Length - 1; x++) {
		
			if(x > low && x < high){

			if (inc_trans[x]<0f){

				// Percorrer os dados do vector - stop se encontrar diferente de zero ou chegar ao fim
				int idx = x + 1;
				while(idx < high && inc_trans[idx]<=0f ){
					idx++;
				}
		
				// Se encontrou um valor diferente de zero
				inc_trans[x] = inc_trans[x-1] + Declive (x-1, idx);
		
			}
		}
	}


		for (int x=0; x < inc_trans.Length - 1; x++) {
			if(inc_trans[x] < min){
				inc_trans[x] = 0f;
			}

			if(inc_trans[x] > max){
				inc_trans[x]=1f;
			}
		}

			funcao_transferencia = inc_trans;


		print ("Function complete");
		sistema_part.SendMessage("ManualFunc", SendMessageOptions.DontRequireReceiver);
	}

	void UpdateFunction() {

		inc_trans = fonte.transf;

		int low = 255;
		int high = 0;
		
		for (int i=0; i<inc_trans.Length; i++) {
			
			if (inc_trans[i] >= 0f && i <= low) {
				low = i;
			}
			
			if (inc_trans[i] >= 0f && i > high) high = i;
		}

		//Aproximate ();

		for (int x=1; x < inc_trans.Length - 1; x++) {
			
			if(x > low && x <= high){
				
				if (inc_trans[x]<0f){
					
					// Percorrer os dados do vector - stop se encontrar diferente de zero ou chegar ao fim
					int idx = x + 1;
					while(idx < high && inc_trans[idx]<=0f ){
						idx++;
					}
					
					// Se encontrou um valor diferente de zero
					inc_trans[x] = inc_trans[x-1] + Declive (x-1, idx);
					
				}
			}
		}

		
		
		for (int x=0; x < inc_trans.Length - 1; x++) {
			if(inc_trans[x] < min){
				inc_trans[x] = 0f;
			}
			
			if(inc_trans[x] > max){
				inc_trans[x]=1f;
			}
		}

		for (int it = low; it <= high; it++) {

			funcao_transferencia[it] = inc_trans[it];
		}

		print ("Update complete");
		sistema_part.SendMessage("ManualFunc", SendMessageOptions.DontRequireReceiver);
	}

	float Declive(int ponto_esq, int ponto_dir){
		int larg = ponto_dir - ponto_esq;
		float alt = inc_trans [ponto_dir] - inc_trans [ponto_esq];
		float decl = alt / larg;
		return decl;
	}

	void Aproximate(){

		for (int i=0; i<inc_trans.Length; i++) {
			if(inc_trans[i] >= 0f){ 
				float val1 = inc_trans[i] * 10;
				int val2 = Mathf.RoundToInt(val1);
				float val3 = val2/10f;
				inc_trans[i]=val3;
			}
		}
	}

}
