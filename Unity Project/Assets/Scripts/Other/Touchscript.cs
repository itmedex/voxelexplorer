﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Touchscript : MonoBehaviour {


	public LayerMask touchInputMask;

	private List<GameObject> touchList = new List<GameObject>();
	private GameObject[] touchesOld;

	public GameObject cursor;

	public int[] func;

	// Toggle Lock
	public bool unlocked = false;
	public Touchscreen tscreen;
	public Noticetext textalert;

	public float xfloat;
	public float yfloat;

	private RaycastHit hit;

//	int[] tempfunc = new int[255];

	void Update () {

		xfloat = Input.mousePosition.x;
		yfloat = Input.mousePosition.y;

		if (unlocked) {

#if UNITY_EDITOR
			if (Input.GetMouseButton (0) || Input.GetMouseButtonUp (0) || Input.GetMouseButtonDown (0)) {
			
				touchesOld = new GameObject[touchList.Count];
				touchList.CopyTo (touchesOld);
				touchList.Clear ();

				
				Ray ray = GetComponent<Camera>().ScreenPointToRay (Input.mousePosition);
				
				if (Physics.Raycast (ray, out hit, touchInputMask)) {
					
					GameObject recipient = hit.transform.gameObject;
					touchList.Add (recipient);
					
					if (Input.GetMouseButtonDown (0)) {
						cursor.SendMessage ("makeLine", SendMessageOptions.DontRequireReceiver);
						recipient.SendMessage ("Follow", hit.point, SendMessageOptions.DontRequireReceiver);
						//recipient.SendMessage ("OnTouchDown", hit.point, SendMessageOptions.DontRequireReceiver);
						//tempfunc = new int[255];
					}

					if (Input.GetMouseButtonUp (0)) {
						//func = tempfunc;
						cursor.SendMessage ("FinishLine", SendMessageOptions.DontRequireReceiver);
						recipient.SendMessage ("Follow", hit.point, SendMessageOptions.DontRequireReceiver);
						//Toggle_Lock();
						//tscreen.Toggle_Lock();
						//textalert.Toggle_text();

						//recipient.SendMessage ("OnTouchUp", hit.point, SendMessageOptions.DontRequireReceiver);
					}

					if (Input.GetMouseButton (0)) {
						cursor.SendMessage ("UpdateLine", SendMessageOptions.DontRequireReceiver);
						//recipient.SendMessage ("OnTouchStay", hit.point, SendMessageOptions.DontRequireReceiver);
						//int xpos = Mathf.RoundToInt(Input.mousePosition.x);
						//int yval = Mathf.RoundToInt(Input.mousePosition.y);


						//tempfunc[xpos]=yval;
					}

				}

				foreach (GameObject g in touchesOld) {
					if (!touchList.Contains (g)) {
						g.SendMessage ("OnTouchExit", hit.point, SendMessageOptions.DontRequireReceiver);
					}
				}
			}
#endif

			if (Input.touchCount > 0) {

				touchesOld = new GameObject[touchList.Count];
				touchList.CopyTo (touchesOld);
				touchList.Clear ();
		

				foreach (Touch touch in Input.touches) {
		
					Ray ray = GetComponent<Camera>().ScreenPointToRay (touch.position);

					if (Physics.Raycast (ray, out hit, touchInputMask)) {

						GameObject recipient = hit.transform.gameObject;
						touchList.Add (recipient);

						if (touch.phase == TouchPhase.Began) {
							recipient.SendMessage ("OnTouchDown", hit.point, SendMessageOptions.DontRequireReceiver);
						}
						if (touch.phase == TouchPhase.Ended) {
							recipient.SendMessage ("OnTouchUp", hit.point, SendMessageOptions.DontRequireReceiver);
						}
						if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved) {
							recipient.SendMessage ("OnTouchStay", hit.point, SendMessageOptions.DontRequireReceiver);
						}
						if (touch.phase == TouchPhase.Canceled) {
							recipient.SendMessage ("OnTouchExit", hit.point, SendMessageOptions.DontRequireReceiver);
						}
					}
				}
				foreach (GameObject g in touchesOld) {
					if (!touchList.Contains (g)) {
						g.SendMessage ("OnTouchExit", hit.point, SendMessageOptions.DontRequireReceiver);
					}
				}
			}
		}
	}

	public void Toggle_Lock (){
		unlocked = !unlocked;
		//print ("lock toggled");
	}
}
