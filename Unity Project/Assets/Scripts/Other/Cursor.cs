﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Cursor : MonoBehaviour {

	// Camera variables
	Camera touchcamera;
	public Camera usecamera;
	//public Touchscript touchscript;
	public Function_draw touchscript;
	private bool drawing = false;
	private bool touched = false;

	// Line Variables
	private GameObject lines;
	private LineRenderer path;
	private int i;

	// Vector das posiçoes do cursor ao longo do desenho
	//largura e altura em inteiros
	int intl;
	int inth;
	//vector para 1D
	private float[] linepos;
	//vector para 2D
	private int[,] perif;
	// Decisao de 1D e 2D
	public bool One_dim;

	// Vector para a funçao de transferencia 1D
	public float[] transf;

	// Vector para a funçao de transferencia 2D
	public int[,] transf2d;

	// Script para criar a funçao de transferencia
	public CustomFunction customs;

	// Script para criar o mapa
	public Mapping mapping;

	// Coordenates of input
	float xfloat;
	float yfloat;
	private Vector3 lastpos;
	private bool began;

	// Normalization measurementes
	Vector3 centralpixel;
	public Vector3 initialcoor;
	public Vector3 finalcoor;

	// Medidas do plano e camara para normalizaçao
	public float screen_lenght;
	public float screen_height;
	public float plane_lenght;
	public float plane_height;

	// Lina que divide o histograma em secçoes
	public GameObject linedivision1;
	public GameObject linedivision2;
	public GameObject linedivision3;
	public GameObject linedivision4;
	public GameObject linedivision5;
	public GameObject linedivision6;
	public GameObject linedivision7;
	public GameObject linedivision8;
	public GameObject linedivision9;
	private GameObject[] grids;
	private LineRenderer grid;
	private bool active = true;

	// plane
	public GameObject plano;

	// adaptar uma funçao livre a um conjunto de caixas
	public bool adapt_1D = false;
	public GameObject multirects;
	public ManageRects rects;

	// Variaveis para fazer alteraçoes a linha
	private bool first;
	private List<Vector3> linelist;
	private float last_change;

	// Nova linha de funçao
	private float[] line_pos;
	private GameObject[] line_fragments;

	void Start () {

		linelist = new List<Vector3> ();
		first = true;

		grids = new GameObject[9];
		grids [0] = linedivision1;
		grids [1] = linedivision2;
		grids [2] = linedivision3;
		grids [3] = linedivision4;
		grids [4] = linedivision5;
		grids [5] = linedivision6;
		grids [6] = linedivision7;
		grids [7] = linedivision8;
		grids [8] = linedivision9;

		touchcamera = usecamera;

		screenBounds ();

		define_line ();

		transform.position = initialcoor;

		last_change = Time.time;

		Generate_func_line ();
	}

	void Update () {

		Update_func_line ();

		if (drawing) {
			Position ();
			CheckPos ();
		}

		if (last_change < Time.time - 1f) {
			FinishInput();
		}

	}

	void UpdateLine (){
			
		last_change = Time.time;

		if (began) {
			lines = (GameObject)Instantiate (Resources.Load ("Provline"));
			i = 2;
			path = lines.GetComponent<LineRenderer> ();
			path.SetVertexCount (i);
			
			path.SetPosition (0, lastpos);
			path.SetPosition (1, transform.position);
			lines.transform.position = lastpos;

			// lista de posiçao
			linelist.Add (lastpos);
			linelist.Add (transform.position);

			lastpos = transform.position;

			// Centrar as coordenadas do cursor
			float xcent = transform.position.x - initialcoor.x;
			float ycent = transform.position.z - initialcoor.z;
		
			
			// Escalar valores de x
			float xescal = (xcent / plane_lenght) * 255;
			// Valor escalado inteiro de x
			int xescalint = Mathf.RoundToInt (xescal);
			//print (xescalint);
			
			// escalar valores de y
			float yescaln = (ycent / plane_height);
			// Valor escalado inteiro de y
			int yesclint = Mathf.RoundToInt (yescaln * 100);

			if (yescaln > linepos [xescalint]) {
				linepos [xescalint] = yescaln;
			}
			perif [xescalint, yesclint] = 1;

			// Update da posiçao alterada
			float yaprox = Aproximate (yescaln);
			yaprox = yaprox * plane_height;
			yaprox = yaprox + initialcoor.z;
		}
	}

	// Creates the object that makes the line

	void makeLine () {

		// Fazer a primeira linha
		last_change = Time.time;
			began = true;
			touched = true;
			drawing = true;
			
		linelist = new List<Vector3> ();

			Position ();

			//Codigo novo que tem em conta o 2D
			linepos = new float[256];
		for (int nr=0; nr<linepos.Length; nr++) {
			linepos[nr] = -1f;
		}
			perif = new int[256, 101];


			lastpos = transform.position;

			// Centrar as coordenadas do cursor
			float xcent = transform.position.x - initialcoor.x;
			float ycent = transform.position.z - initialcoor.z;
		
			// Escalar valores de x
			float xescal = (xcent / plane_lenght) * 255;
			// Valor escalado inteiro de x
			int xescalint = Mathf.RoundToInt (xescal);

			// escalar valores de y
			float yescaln = (ycent / plane_height);
			// Valor escalado inteiro de y
			int yesclint = Mathf.RoundToInt (yescaln * 100);

			linepos [xescalint] = yescaln;
			perif [xescalint, yesclint] = 1;


	}

	// Criar o vector da funçao

	void FinishInput(){
		began = false;
		drawing = false;
		erase_prov_line ();
	}

	void FinishLine(){

		last_change = Time.time;

		if (began) {
			began = false;
			drawing = false;

			if(false){ //!modify (linepos, 0.5f)
				if (adapt_1D) {
					transf = new float[256];
					transf = linepos;
					//print ("desenhou e mandou");
					//erase_line ();
					multirects.SetActive (true);
					rects.SendMessage ("Adapt_1D", SendMessageOptions.DontRequireReceiver);
				} else {
					//print ("dobled line");
					transf = new float[256];
					transf = linepos;
					/*erase_line();
					for(int f = 0; f < linelist.Count; f=f+2)
					{
						lines = (GameObject)Instantiate(Resources.Load("Line"));
						path = lines.GetComponent<LineRenderer> ();
						path.SetVertexCount (2);
						path.SetPosition (0, linelist[f]);
						path.SetPosition(1,linelist[f+1]);
						lines.transform.position = linelist[f];
					}*/
					erase_prov_line();
					customs.SendMessage ("Fullfunction", SendMessageOptions.DontRequireReceiver);
				}
			}else{
				// mandamos ao Custom function fazer update da funçao de tr
				transf = new float[256];
				transf = linepos;
				/*
				// determinar os limites da nova linha
				float limit_left;
				float limit_right;
				limit_left = linelist[0].x;
				limit_right = linelist[0].x;
				for(int f = 0; f < linelist.Count; f++)
				{
					if( linelist[f].x < limit_left) limit_left = linelist[f].x;

					if( linelist[f].x > limit_right) limit_right = linelist[f].x;
				}
				// Apagar os segmentos da linha anterior
				GameObject[] linhas = GameObject.FindGameObjectsWithTag("funtionline");
				foreach (GameObject linha in linhas) {
					if(linha.transform.position.x > limit_left && linha.transform.position.x < limit_right){
						Destroy ( linha.gameObject);
					}
				}
				// Actualizar para linha nova
				for(int f = 0; f < linelist.Count; f=f+2)
				{
					lines = (GameObject)Instantiate(Resources.Load("Line"));
					path = lines.GetComponent<LineRenderer> ();
					path.SetVertexCount (2);
					path.SetPosition (0, linelist[f]);
					path.SetPosition(1,linelist[f+1]);
					lines.transform.position = linelist[f];
				}*/
				erase_prov_line();
				customs.SendMessage ("UpdateFunction", SendMessageOptions.DontRequireReceiver);
			}

		}
	}

	// funçao a ser mudada de script

	bool modify (float[] linepos, float percent){


		float min = 255f;
		float max = 0f;

		for (int i=0; i<linepos.Length; i++) {

			if (linepos[i] >= 0f && i <= min) {
				min = i;
			}

			if (linepos[i] >= 0f && i > max) max = i;
		}

		float perc = (max - min) / 256f;
		/*
		print (max);
		print (min);
		print ("run modify " + perc);
		*/
		if (perc > percent) {
			return false;
		} else {
			return true;
		}
	}

	public void screenBounds () {

		Vector3 screenPosfi = touchcamera.WorldToScreenPoint(touchcamera.transform.position);
		
		centralpixel = screenPosfi;

		screen_height = touchcamera.pixelHeight;
		screen_lenght = touchcamera.pixelWidth;

		initialcoor = touchcamera.ScreenToWorldPoint (new Vector3(centralpixel.x - screen_lenght / 2, centralpixel.y - screen_height / 2, 0f));
		finalcoor = touchcamera.ScreenToWorldPoint (new Vector3(centralpixel.x + screen_lenght / 2, centralpixel.y + screen_height / 2, 0f));

		plane_height = finalcoor.z - initialcoor.z;
		plane_lenght = finalcoor.x - initialcoor.x;
	}

	void scale_plane(){
	
		Vector3 scale;

		scale.x = plane_lenght;
		scale.y = plane_height;
		scale.z = 0.01f;

		plano.transform.localScale = scale;
	}

	// Determina a posiçao do cursor

	void Position(){

		xfloat = touchscript.xfloat;
		yfloat = touchscript.yfloat;
		
		Vector3 worldPos = touchcamera.ScreenToWorldPoint(new Vector3(xfloat, yfloat, 0));
		
		worldPos.y = 3.5f;;
		
		transform.position = worldPos;
	}

	void CheckPos(){
		
		Vector3 newpos = transform.position;
		
		if (transform.position.x < initialcoor.x) {
			newpos.x = initialcoor.x;
		}
		
		if (transform.position.z > finalcoor.z) {
			newpos.z = finalcoor.z;
		}
		if (transform.position.x > finalcoor.x) {
			newpos.x = finalcoor.x;
		}
		if (transform.position.z < initialcoor.z) {
			newpos.z = initialcoor.z;
		}
		
		if (newpos != transform.position) {
			transform.position = newpos;
		}
		
	}

	float Aproximate(float point){
			float val1 = point * 10;
			int val2 = Mathf.RoundToInt(val1);
			float val3 = val2/10f;
			return val3;

	}

	void erase_line(){

		GameObject[] linhas = GameObject.FindGameObjectsWithTag("funtionline");
		foreach (GameObject linha in linhas) {
			Destroy (linha.gameObject);
		}
		touched = false;
	}

	void erase_prov_line(){
		
		GameObject[] linhas = GameObject.FindGameObjectsWithTag("provline");
		foreach (GameObject linha in linhas) {
			Destroy (linha.gameObject);
		}
		touched = false;
	}


	void define_line(){

		float step = plane_lenght / (grids.Length+1);

		for (int i=0; i < grids.Length; i++) {

			grid = grids[i].GetComponent <LineRenderer> ();
			grid.SetVertexCount (2);

			grid.SetPosition (0, new Vector3 (initialcoor.x + step*(i+1), 3f, finalcoor.z));
			grid.SetPosition (1, new Vector3 (initialcoor.x + step*(i+1), 3f, initialcoor.z));
		}
	}

	void Generate_func_line(){

		line_pos = new float[100];

		line_fragments = new GameObject[99];

		Vector3 lastcoor = initialcoor;
		lastcoor.y = 3.5f;
		Vector3 newcoor;

		for (int i = 0; i<99; i++) {

			GameObject seglin = (GameObject)Instantiate (Resources.Load ("Line"));
			line_fragments[i]= seglin;

			int nr = 2;
			LineRenderer segpath = seglin.GetComponent<LineRenderer> ();
			segpath.SetVertexCount (nr);

			newcoor = lastcoor;
			newcoor.x += plane_lenght/99f;

			segpath.SetPosition (0, lastcoor);
			segpath.SetPosition (1, newcoor);

			lastcoor = newcoor;

		}
	}

	void Update_func_line(){

		float[] transfer = customs.funcao_transferencia;
		float domain = transfer.Length;



		for (int i=0; i < line_pos.Length; i++) {

			int nr = Mathf.RoundToInt((i/100f)*domain);

			line_pos[i] = transfer[nr];
		}

		for (int i = 0; i<99; i++) {

			GameObject linha = line_fragments[i];
			LineRenderer desenho = linha.GetComponent<LineRenderer> ();
			desenho.SetVertexCount (2);

			Vector3 first = initialcoor;
			Vector3 second = initialcoor;

			first.x = first.x + i*(plane_lenght/99f);
			first.y = 3.5f;
			first.z = first.z + line_pos[i]*plane_height;

		 	second.x = second.x + (i+1)*(plane_lenght/99f);
			second.y = 3.5f;
			second.z = second.z + line_pos[i+1]*plane_height;
			
			desenho.SetPosition (0, first);
			desenho.SetPosition (1, second);

		}

	}


}
