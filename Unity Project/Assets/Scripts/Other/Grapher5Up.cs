﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class Grapher5Up : MonoBehaviour {

	// Controlo de resoluçao 
	private float resolution;
	[Range(0, 1)]
	public float main_resolution;
	[Range(0, 1)]
	public float thumb_resolution;
	public float scale;

	// Raio das particulas
	[Range(0.001f, 1f)]
	public float radius = 0.1f;

	// Espaçamento entre fatias principais
	[Range(0.001f, 1f)]
	public float spacing = 0.1f;

	// Medidas da figura
	Texture2D figura;
	public int altura = 0;
	public int largura = 0;
	
	public int fatias;
	int i=0;
	public Color[,,] cores;
	string[] slices;  

	// Vector de sistema de particulas inalterado
	public ParticleSystem.Particle[] original_points;
	// Vector de sistema de particulas alterado
	private ParticleSystem.Particle[] points;
	// Coordenadas originais de cada ponto
	public Vector3[] original_point;

	// Multiview 
	public Multiview mlt;
	public ParticleSystem.Particle[] thumb_points;
	
	// Classe da funçao de transferencia manual
	public CustomFunction CFT;
	public float[] ftm;

	// Classe da funçao de transferencia linear
	public Linear_line_draw FTL;
	public float[] ftl;

	// Classe da funçao de transferencia por sliders
	public ManageRects STF;
	public float[] stf;

	// Fonte de funçao de transferencia 2D
	public Mapping mapping;
	public int[,] ft2d;

	// Classe fonte da funçao de transferencia
	public control_window CW;
	public float[] swt;

	// Classe da funçao de cores
	public bool colored;
	public Color_Generator CG;
	public ColorFunc2 NCG;

	// Matrix de Gradientes - So usado para 2D. Nao usado agora
	//Vector
	Vector3[,,] grad_mat;
	//Escalar
	float[,,] grad_scal;
	public int[,,] grad_scal_int;
	// Selecçao tem cor
	public bool grad_cor;

	// Forward gradient
	public bool forward;

	// Vector do histograma
	public float[] histograma;
	public Histogram desenho_hist;

	// Selecçao de modelo
	private string model;

	// Estado inicial
	private Vector3 initialpos;
	private Quaternion initialrot;

	// Medida original
	public float dist_ref;
	

	void Start () {


		initialpos = transform.position;
		initialrot = transform.rotation;

		GameObject objinf = GameObject.Find ("Transition");
		if (objinf == null) {
			resolution = thumb_resolution;
			Load_cranio_kinect ();
		} else {
			Transitioninfo info = objinf.GetComponent<Transitioninfo> ();
			model = info.selected_model;

			if(info.first){
				resolution = main_resolution;
			}else{
				resolution = thumb_resolution;
			}
		}

		if(model == "tibia"){
			Load_tibia ();
		}
		if(model == "cranio"){
			Load_cranio ();
		}
		if(model == "torax"){
			Load_torax ();
		}
		if(model == "tronco"){
			Load_tronco ();
		}
		if(model == "teste1"){
			Load_teste_1 ();
		}
		if(model == "teste2"){
			Load_teste_2 ();
		}
		if(model == "teste3"){
			Load_teste_3 ();
		}

	}



	// Cria o modelo 3D

	private void CreatePoints () {

		float incrementx;
		float incrementy;
		float incrementz;

		// Sampling rate
		int intincx = Mathf.RoundToInt (1f/resolution);
		int intincy = Mathf.RoundToInt (1f/resolution);

		// Determinar espaçamento das particulas
		if (largura >= altura) {
			incrementx = scale / ((largura*resolution) - 1);
			incrementy = incrementx;
		}
		else{
			incrementy = scale / ((altura*resolution) - 1);
			incrementx = incrementy;
		}

		// Determinar tamanho ds particulas 
		radius = 2*incrementx;

		int nr_rep = Mathf.RoundToInt ((spacing - radius) / radius);
		if (nr_rep < 0)
			nr_rep = 0;
		//print (nr_rep);

		incrementz = spacing * scale;

		int i = 0;
		for (int z = 0; z < fatias; z++) {
			for(int rep = 0; rep <= nr_rep; rep++){
				for (int x = 0; x < largura; x = x + intincx) {
					for (int y = 0; y < altura; y = y + intincy) {
						i++;
					}
				}
			}
		}

		points = new ParticleSystem.Particle[i];
		original_points = new ParticleSystem.Particle[i];
		original_point = new Vector3[i];

		i = 0;
		for (int z = 0; z < fatias; z++) {
			for(int rep = 0; rep <= nr_rep; rep++){
			for (int x = 0; x < largura; x = x + intincx) {
				for (int y = 0; y < altura; y = y + intincy) {

					Vector3 p = new Vector3(x, y, z);

					original_point[i] = p;

					p.x = (p.x*resolution) * incrementx - scale/2f;
					p.y = (p.y*resolution) * incrementy - scale/2f;
						p.z = p.z * incrementz +(rep*(incrementz/(nr_rep+1))) - (fatias*incrementz/2f);

					points[i].position = p;

					points[i].color = cores[x,y,z];

					points[i].size = radius;

					original_points[i] = points[i];
					i++;

				}
			}
			}
		}

		transform.localScale = new Vector3 (scale, scale, fatias * incrementz);

			
		for (int j = 0; j < points.Length; j++) {
			Color c = points [j].color;
			c.a = Still (points [j].position, c);
			points [j].color = c;
		}

		GetComponent<ParticleSystem>().SetParticles(points, points.Length);

	}


	private void CreatePoints_Thumb () {
		
		float incrementx;
		float incrementy;
		float incrementz;
		
		// Sampling rate
		int intincx = Mathf.RoundToInt (1f/thumb_resolution);
		int intincy = Mathf.RoundToInt (1f/thumb_resolution);
		
		// Determinar espaçamento das particulas
		if (largura >= altura) {
			incrementx = scale / ((largura*thumb_resolution) - 1);
			incrementy = incrementx;
		}
		else{
			incrementy = scale / ((altura*thumb_resolution) - 1);
			incrementx = incrementy;
		}
		
		// Determinar tamanho ds particulas 
		radius = 2*incrementx;
		
		int nr_rep = Mathf.RoundToInt ((spacing - radius) / radius);
		if (nr_rep < 0)
			nr_rep = 0;
		//print (nr_rep);
		
		incrementz = spacing * scale;
		
		int i = 0;
		for (int z = 0; z < fatias; z++) {
			for(int rep = 0; rep <= nr_rep; rep++){
				for (int x = 0; x < largura; x = x + intincx) {
					for (int y = 0; y < altura; y = y + intincy) {
						i++;
					}
				}
			}
		}
		
		thumb_points = new ParticleSystem.Particle[i];
		
		i = 0;
		for (int z = 0; z < fatias; z++) {
			for(int rep = 0; rep <= nr_rep; rep++){
				for (int x = 0; x < largura; x = x + intincx) {
					for (int y = 0; y < altura; y = y + intincy) {
						
						Vector3 p = new Vector3(x, y, z);
						
						p.x = (p.x*thumb_resolution) * incrementx - scale/2f;
						p.y = (p.y*thumb_resolution) * incrementy - scale/2f;
						p.z = p.z * incrementz +(rep*(incrementz/(nr_rep+1))) - (fatias*incrementz/2f);
						
						thumb_points[i].position = p;
						
						thumb_points[i].color = cores[x,y,z];
						
						thumb_points[i].size = radius;

						i++;
						
					}
				}
			}
		}
	}


	// Funcoes de opacidade

	// Funçao manual

	void ManualFunc(){
		
		ftm = CFT.funcao_transferencia;
		
		for (int i = 0; i < points.Length; i++) {
			// Vai buscar intensidade original
			Vector3 po = original_point[i];
			int x = Mathf.RoundToInt(po.x);
			int y = Mathf.RoundToInt(po.y);
			int z = Mathf.RoundToInt(po.z);
			Color c = cores[x, y, z];
			// Cor do modelo
			Color c_m = points [i].color;

			int gray_value = Mathf.FloorToInt(255*(c.r + c.g + c.b)/3);
			//print(gray_value);
			c_m.a = ftm[gray_value];
			//print (c.a);
			points [i].color = c_m;
			original_points[i].color = c_m;
		}
		
		GetComponent<ParticleSystem>().SetParticles(points, points.Length);
	}

	void Window_func(){
		
		swt = CW.transfer_func;
		
		for (int i = 0; i < points.Length; i++) {
			// Vai buscar intensidade original
			Vector3 po = original_point[i];
			int x = Mathf.RoundToInt(po.x);
			int y = Mathf.RoundToInt(po.y);
			int z = Mathf.RoundToInt(po.z);
			Color c = cores[x, y, z];
			// Cor do modelo
			Color c_m = points [i].color;
			
			int gray_value = Mathf.FloorToInt(255*(c.r + c.g + c.b)/3);

			c_m.a = swt[gray_value];

			points [i].color = c_m;
			original_points[i].color = c_m;
		}
		
		GetComponent<ParticleSystem>().SetParticles(points, points.Length);
	}

	void Slider_func(){
		
		stf = STF.trans_func;
		
		for (int i = 0; i < points.Length; i++) {
			// Vai buscar intensidade original
			Vector3 po = original_point[i];
			int x = Mathf.RoundToInt(po.x);
			int y = Mathf.RoundToInt(po.y);
			int z = Mathf.RoundToInt(po.z);
			Color c = cores[x, y, z];
			// Cor do modelo
			Color c_m = points [i].color;
			
			int gray_value = Mathf.FloorToInt(255*(c.r + c.g + c.b)/3);

			c_m.a = stf[gray_value];

			points [i].color = c_m;
			original_points[i].color = c_m;
		}
		
		GetComponent<ParticleSystem>().SetParticles(points, points.Length);

	}
	void Linear_Func(){
		
		ftl = FTL.Func_trans;
		
		for (int i = 0; i < points.Length; i++) {
			// Vai buscar intensidade original
			Vector3 po = original_point[i];
			int x = Mathf.RoundToInt(po.x);
			int y = Mathf.RoundToInt(po.y);
			int z = Mathf.RoundToInt(po.z);
			Color c = cores[x, y, z];
			// Cor do modelo
			Color c_m = points [i].color;
			int gray_value = Mathf.FloorToInt(255*(c.r + c.g + c.b)/3);
			//print(gray_value);
			c_m.a = ftl[gray_value];
			//print (c.a);
			points [i].color = c_m;
			original_points[i].color = c_m;
		}
		
		GetComponent<ParticleSystem>().SetParticles(points, points.Length);
		
	}

	void Apply_function( float[] transfer){
		
		for (int i = 0; i < points.Length; i++) {
			// Vai buscar intensidade original
			Vector3 po = original_point[i];
			int x = Mathf.RoundToInt(po.x);
			int y = Mathf.RoundToInt(po.y);
			int z = Mathf.RoundToInt(po.z);
			Color c = cores[x, y, z];
			// Cor do modelo
			Color c_m = points [i].color;
			int gray_value = Mathf.FloorToInt(255*(c.r + c.g + c.b)/3);
			//print(gray_value);
			c_m.a = transfer[gray_value];
			//print (c.a);
			points [i].color = c_m;
			original_points[i].color = c_m;
		}
		
		GetComponent<ParticleSystem>().SetParticles(points, points.Length);
		
	}



	// Funçoes Lineares
	
	private float Still (Vector3 p, Color c){
		if ((c.r + c.g + c.b)/3 <= 0.1f) {
			return 0f;
		}
		else
		return 1f;
	}

	// Gradientes

	void GenerateGradients(int sampleSize){

		// tenho de criar uma matrix para os gradientes
		grad_mat = new Vector3[largura, altura, fatias];
		grad_scal = new float[largura, altura, fatias];
		grad_scal_int = new int[largura, altura, fatias];

		int n = sampleSize;
		float minval = 0f;
		float maxval = 0f;
		Vector3 s1, s2;
		float s;

		for (int z = 0; z < fatias; z++)
		{
			for (int y = 0; y < altura; y++)
			{
				for (int x = 0; x < largura; x++)
				{
					s = GetIntens(x,y,z);
					s1.x = GetIntens(x - n, y, z);
					s2.x = GetIntens(x + n, y, z);
					s1.y = GetIntens(x, y - n, z);
					s2.y = GetIntens(x, y + n, z);
					s1.z = GetIntens(x, y, z - n);
					s2.z = GetIntens(x, y, z + n);
					
					grad_mat[x,y,z] = Vector3.Normalize(s2 - s1);

					if(forward){
						grad_scal[x,y,z] = (s2.x-s)+(s2.y-s)+(s2.z-s);
					}else{
					grad_scal[x,y,z] = (s2.x-s1.x)+(s2.y-s1.y)+(s2.z-s1.z);
					}

					if (grad_scal[x,y,z]>maxval){
						maxval = grad_scal[x,y,z];
					}
					if (grad_scal[x,y,z]<minval){
						minval = grad_scal[x,y,z];
					}

				}
			}
		}

		for (int z = 0; z < fatias; z++)
		{
			for (int y = 0; y < altura; y++)
			{
				for (int x = 0; x < largura; x++)
				{
					float scalval = grad_scal[x,y,z];
					scalval = ((scalval - minval) / (maxval-minval))*100;
					grad_scal_int[x,y,z] = Mathf.RoundToInt(scalval);
				}
			}
		}

	}

	float GetIntens ( int x, int y, int z){

		// Corrigir coordenadas

		int x_1 = x;
		int y_1 = y;
		int z_1 = z;

		if (x < 0) {
			x_1 = 0;
		}

		if (x >= largura) {
			x_1 = largura-1;
		}

		if (y < 0) {
			y_1 = 0;
		}
			
		if (y >= altura) {
			y_1 = altura-1;
		}

		if (z < 0) {
			z_1 = 0;
		}
			
		if (z >= fatias) {
			z_1 = fatias-1;
		}



		// determinar intesidade
		Color co = cores [x_1, y_1, z_1];

		float co_intens = ((co.r + co.b + co.g) / 3);

		return co_intens;
	}

	public void Reset_model(){
		CreatePoints();
		for (int i = 0; i < points.Length; i++) {
			Color c = points [i].color;
			c.a = Still (points [i].position, c);
			points [i].color = c;
		}
		GetComponent<ParticleSystem>().SetParticles(points, points.Length);
	}


	void Clip_plane ( Vector3[] point_positions) {


		Vector3 pos1 = point_positions [0];
		Vector3 pos2 = point_positions [1];
		Vector3 pos3 = point_positions [2];

		GameObject point1 = new GameObject ();
		point1.transform.position = pos1;
		point1.transform.parent = transform;

		GameObject point2 = new GameObject ();
		point2.transform.position = pos2;
		point2.transform.parent = transform;

		GameObject point3 = new GameObject ();
		point3.transform.position = pos3;
		point3.transform.parent = transform;

		Vector3 plane_pos = (point1.transform.localPosition + point2.transform.localPosition + point3.transform.localPosition) / 3f;
		print (plane_pos);

		Vector3 side1 = point2.transform.localPosition - point1.transform.localPosition;
		Vector3 side2 = point3.transform.localPosition - point1.transform.localPosition;
		
		Vector3 v1 = Vector3.Cross(side1,side2);
		float perpLength1 = v1.magnitude;
		v1 /= perpLength1;

		for (int i = 0; i < points.Length; i++) {

			Vector3 pos = points [i].position;

			Vector3 v2 = (pos - plane_pos);
			float perpLength2 = v2.magnitude;
			v2 /= perpLength2;

			float dotp = Vector3.Dot(v1,v2);

			if(dotp > 0){
				Color c = points [i].color;
				c.a = 1f;
				points [i].color = c;
			}else{
				Color c = points [i].color;
				c.a = 0f;
				points [i].color = c;
			}
		}

		for (int j = 0; j < points.Length; j++) {
			Color c = points [j].color;
			if( Still (points [j].position, c)==0f) c.a = 0f; 
			points [j].color = c;
		}

		GetComponent<ParticleSystem>().SetParticles(points, points.Length);

		Destroy (point1);
		Destroy (point2);
		Destroy (point3);
	}

	void Clip_box ( Vector3 point) {

		GameObject point1 = new GameObject ();
		point1.transform.position = point;
		point1.transform.parent = transform;

		for (int i = 0; i < points.Length; i++) {
			
			Vector3 pos = points [i].position;
			
			if(pos.x < point1.transform.position.x && pos.y < point1.transform.position.y && pos.z > point1.transform.position.z){
				Color c = points [i].color;
				c.a = 1f;
				points [i].color = c;
			}else{
				Color c = points [i].color;
				c.a = 0f;
				points [i].color = c;
			}
		}
		
		for (int j = 0; j < points.Length; j++) {
			Color c = points [j].color;
			if( Still (points [j].position, c)==0f) c.a = 0f; 
			points [j].color = c;
		}
		
		GetComponent<ParticleSystem>().SetParticles(points, points.Length);
		
		Destroy (point1);
	}


	void Load_cranio_kinect(){
		
		fatias = 28;
		dist_ref = 25f;
		
		slices = new string[fatias];
		
		for( int nr = 1; nr < fatias + 1; nr++ ) {
			string myString = nr.ToString();
			slices [nr-1] = myString;
		}
		// Obtem as medidas das imagens
		
		figura = (Texture2D)Resources.Load("cranio/" + slices[0]);
		altura = figura.height;
		largura = figura.width;
		cores = new Color[largura,altura,fatias];
		
		
		// Cria o vector cores
		for (int z = 0; z < fatias; z++) {
			figura = (Texture2D)Resources.Load ("cranio/" + slices[z]);
			for (int x = 0; x < largura; x++) {
				for (int y = 0; y < altura; y++) {
					cores [x,y,z] = figura.GetPixel (x, y);
					i++;
				}
			}
		}
		
		CreatePoints();
	}

	void Load_cranio(){

		fatias = 28;
		dist_ref = 25f;
		
		slices = new string[fatias];
		
		for( int nr = 1; nr < fatias + 1; nr++ ) {
			string myString = nr.ToString();
			slices [nr-1] = myString;
		}
		// Obtem as medidas das imagens
		
		figura = (Texture2D)Resources.Load("cranio/" + slices[0]);
		altura = figura.height;
		largura = figura.width;
		cores = new Color[largura,altura,fatias];
		
		
		// Cria o vector cores
		for (int z = 0; z < fatias; z++) {
			figura = (Texture2D)Resources.Load ("cranio/" + slices[z]);
			for (int x = 0; x < largura; x++) {
				for (int y = 0; y < altura; y++) {
					cores [x,y,z] = figura.GetPixel (x, y);
					i++;
				}
			}
		}

		// Criar o Vector do histograma
		histograma = new float[256];
		
		for (int z = 0; z < fatias; z++) {
			for (int x = 0; x < largura; x++) {
				for (int y = 0; y < altura; y++) {
					int intval = Mathf.RoundToInt(GetIntens(x,y,z)*255);
					histograma[intval] = histograma[intval] + 1f;
				}
			}
		}
		
		histograma [0] = 0;

		desenho_hist.SendMessage("Create_Histo", SendMessageOptions.DontRequireReceiver);

		CreatePoints();
		CreatePoints_Thumb ();
		
		mlt.SendMessage("Create_views", SendMessageOptions.DontRequireReceiver);

	}

	void Load_tibia(){

		fatias = 17;
		
		slices = new string[fatias];
		
		for( int nr = 2; nr < fatias + 2; nr++ ) {
			string myString = nr.ToString();
			slices [nr-2] = myString;
		}
		// Obtem as medidas das imagens
		
		figura = (Texture2D)Resources.Load(slices[0]);
		altura = figura.height;
		largura = figura.width;
		cores = new Color[largura,altura,fatias];
		
		
		// Cria o vector cores
		for (int z = 0; z < fatias; z++) {
			figura = (Texture2D)Resources.Load (slices[z]);
			for (int x = 0; x < largura; x++) {
				for (int y = 0; y < altura; y++) {
					cores [x,y,z] = figura.GetPixel (x, y);
					i++;
				}
			}
		}
		
		// Criar o Vector do histograma
		histograma = new float[256];
		
		for (int z = 0; z < fatias; z++) {
			for (int x = 0; x < largura; x++) {
				for (int y = 0; y < altura; y++) {
					int intval = Mathf.RoundToInt(GetIntens(x,y,z)*255);
					histograma[intval] = histograma[intval] + 1f;
				}
			}
		}
		
		histograma [0] = 0;

		desenho_hist.SendMessage("Create_Histo", SendMessageOptions.DontRequireReceiver);
		
		CreatePoints();
		CreatePoints_Thumb ();
		
		mlt.SendMessage("Create_views", SendMessageOptions.DontRequireReceiver);
	}


	void Load_torax(){
		
		fatias = 36;
		
		slices = new string[fatias];
		
		for( int nr = 1; nr < fatias + 1; nr++ ) {
			string myString = nr.ToString();
			slices [nr-1] = myString;
		}
		// Obtem as medidas das imagens
		
		figura = (Texture2D)Resources.Load(slices[0]);
		altura = figura.height;
		largura = figura.width;
		cores = new Color[largura,altura,fatias];
		
		
		// Cria o vector cores
		for (int z = 0; z < fatias; z++) {
			figura = (Texture2D)Resources.Load ("torax/"+slices[z]);
			for (int x = 0; x < largura; x++) {
				for (int y = 0; y < altura; y++) {
					cores [x,y,z] = figura.GetPixel (x, y);
					i++;
				}
			}
		}
		
		// Criar o Vector do histograma
		histograma = new float[256];
		
		for (int z = 0; z < fatias; z++) {
			for (int x = 0; x < largura; x++) {
				for (int y = 0; y < altura; y++) {
					int intval = Mathf.RoundToInt(GetIntens(x,y,z)*255);
					histograma[intval] = histograma[intval] + 1f;
				}
			}
		}
		
		histograma [0] = 0;
	
		desenho_hist.SendMessage("Create_Histo", SendMessageOptions.DontRequireReceiver);
		
		CreatePoints();
		CreatePoints_Thumb ();
		
		mlt.SendMessage("Create_views", SendMessageOptions.DontRequireReceiver);
	}

	void Load_tronco(){
		
		fatias = 60;
		
		slices = new string[fatias];
		
		for( int nr = 1; nr < fatias + 1; nr++ ) {
			string myString = nr.ToString();
			slices [nr-1] = myString;
		}
		// Obtem as medidas das imagens
		
		figura = (Texture2D)Resources.Load(slices[0]);
		altura = figura.height;
		largura = figura.width;
		cores = new Color[largura,altura,fatias];
		
		
		// Cria o vector cores
		for (int z = 0; z < fatias; z++) {
			figura = (Texture2D)Resources.Load ("tronco/"+slices[z]);
			for (int x = 0; x < largura; x++) {
				for (int y = 0; y < altura; y++) {
					cores [x,y,z] = figura.GetPixel (x, y);
					i++;
				}
			}
		}
		
		// Criar o Vector do histograma
		histograma = new float[256];
		
		for (int z = 0; z < fatias; z++) {
			for (int x = 0; x < largura; x++) {
				for (int y = 0; y < altura; y++) {
					int intval = Mathf.RoundToInt(GetIntens(x,y,z)*255);
					histograma[intval] = histograma[intval] + 1f;
				}
			}
		}
		
		histograma [0] = 0;
		
		desenho_hist.SendMessage("Create_Histo", SendMessageOptions.DontRequireReceiver);
		
		CreatePoints();
		CreatePoints_Thumb ();
		
		mlt.SendMessage("Create_views", SendMessageOptions.DontRequireReceiver);
	}

	void Load_teste_1(){
		
		fatias = 23;
		
		slices = new string[fatias];
		
		for( int nr = 1; nr < fatias + 1; nr++ ) {
			string myString = nr.ToString();
			slices [nr-1] = myString;
		}
		// Obtem as medidas das imagens
		
		figura = (Texture2D)Resources.Load(slices[0]);
		altura = figura.height/2;
		largura = figura.width/2;
		cores = new Color[largura,altura,fatias];
		
		
		// Cria o vector cores
		for (int z = 0; z < fatias; z++) {
			figura = (Texture2D)Resources.Load ("Tests 1/"+slices[z]);
			for (int x = 0; x < largura; x++) {
				for (int y = 0; y < altura; y++) {
					cores [x,y,z] = figura.GetPixel (x, y);
					i++;
				}
			}
		}
		
		// Criar o Vector do histograma
		histograma = new float[256];
		
		for (int z = 0; z < fatias; z++) {
			for (int x = 0; x < largura; x++) {
				for (int y = 0; y < altura; y++) {
					int intval = Mathf.RoundToInt(GetIntens(x,y,z)*255);
					histograma[intval] = histograma[intval] + 1f;
				}
			}
		}
		
		histograma [0] = 0;
		
		desenho_hist.SendMessage("Create_Histo", SendMessageOptions.DontRequireReceiver);
		
		CreatePoints();
		CreatePoints_Thumb ();
		
		mlt.SendMessage("Create_views", SendMessageOptions.DontRequireReceiver);
	}

	void Load_teste_2(){
		
		fatias = 22;
		
		slices = new string[fatias];
		
		for( int nr = 1; nr < fatias + 1; nr++ ) {
			string myString = nr.ToString();
			slices [nr-1] = myString;
		}
		// Obtem as medidas das imagens
		
		figura = (Texture2D)Resources.Load(slices[0]);
		altura = figura.height;
		largura = figura.width;
		cores = new Color[largura,altura,fatias];
		
		
		// Cria o vector cores
		for (int z = 0; z < fatias; z++) {
			figura = (Texture2D)Resources.Load ("Tests 2/"+slices[z]);
			for (int x = 0; x < largura; x++) {
				for (int y = 0; y < altura; y++) {
					cores [x,y,z] = figura.GetPixel (x, y);
					i++;
				}
			}
		}
		
		// Criar o Vector do histograma
		histograma = new float[256];
		
		for (int z = 0; z < fatias; z++) {
			for (int x = 0; x < largura; x++) {
				for (int y = 0; y < altura; y++) {
					int intval = Mathf.RoundToInt(GetIntens(x,y,z)*255);
					histograma[intval] = histograma[intval] + 1f;
				}
			}
		}
		
		histograma [0] = 0;
		
		desenho_hist.SendMessage("Create_Histo", SendMessageOptions.DontRequireReceiver);
		
		CreatePoints();
		CreatePoints_Thumb ();
		
		mlt.SendMessage("Create_views", SendMessageOptions.DontRequireReceiver);
	}

	void Load_teste_3(){
		
		fatias = 30;
		
		slices = new string[fatias];
		
		for( int nr = 1; nr < fatias + 1; nr++ ) {
			string myString = nr.ToString();
			slices [nr-1] = myString;
		}
		// Obtem as medidas das imagens
		
		figura = (Texture2D)Resources.Load(slices[0]);
		altura = figura.height;
		largura = figura.width;
		cores = new Color[largura,altura,fatias];
		
		
		// Cria o vector cores
		for (int z = 0; z < fatias; z++) {
			figura = (Texture2D)Resources.Load ("Tests 3/"+slices[z]);
			for (int x = 0; x < largura; x++) {
				for (int y = 0; y < altura; y++) {
					cores [x,y,z] = figura.GetPixel (x, y);
					i++;
				}
			}
		}
		
		// Criar o Vector do histograma
		histograma = new float[256];
		
		for (int z = 0; z < fatias; z++) {
			for (int x = 0; x < largura; x++) {
				for (int y = 0; y < altura; y++) {
					int intval = Mathf.RoundToInt(GetIntens(x,y,z)*255);
					histograma[intval] = histograma[intval] + 1f;
				}
			}
		}
		
		histograma [0] = 0;
		
		desenho_hist.SendMessage("Create_Histo", SendMessageOptions.DontRequireReceiver);
		
		CreatePoints();
		CreatePoints_Thumb ();
		
		mlt.SendMessage("Create_views", SendMessageOptions.DontRequireReceiver);
	}
	public void Reset_pos(){

		transform.position = initialpos;
		transform.rotation = initialrot;
	}

	public void Reset_system(){

		CreatePoints();

		for (int i = 0; i < points.Length; i++) {
			Color c = points [i].color;
			c.a = Still (points [i].position, c);
			points [i].color = c;
		}

		GetComponent<ParticleSystem>().SetParticles(points, points.Length);
	}

	// aplicar cores

	void Apply_color(){

		Color[] colors = CG.colors;
		int[] set = CG.defined;
		
		for (int i = 0; i < points.Length; i++) {
			// Vai buscar intensidade original
			Vector3 po = original_point[i];
			int x = Mathf.RoundToInt(po.x);
			int y = Mathf.RoundToInt(po.y);
			int z = Mathf.RoundToInt(po.z);
			Color c = cores[x, y, z];
			// Cor do modelo
			Color c_m = points [i].color;
			float alpha = c_m.a;
			
			int gray_value = Mathf.FloorToInt(255*(c.r + c.g + c.b)/3);

			if(set[gray_value]==1){
			Color nc = colors[gray_value];
			nc.a = alpha;
			points [i].color = nc;
			}else{
				c.a = alpha;
				points[i].color = c;
			}
		}
		
		GetComponent<ParticleSystem>().SetParticles(points, points.Length);


	}

	public void New_scale(float nscale){

		scale = nscale;

		float incrementx;
		float incrementy;
		float incrementz;
		
		// Sampling rate
		int intincx = Mathf.RoundToInt (1f/resolution);
		int intincy = Mathf.RoundToInt (1f/resolution);
		
		// Determinar espaçamento das particulas
		if (largura >= altura) {
			incrementx = scale / ((largura*resolution) - 1);
			incrementy = incrementx;
		}
		else{
			incrementy = scale / ((altura*resolution) - 1);
			incrementx = incrementy;
		}
		
		// Determinar tamanho ds particulas 
		radius = 2*incrementx;
		
		int nr_rep = Mathf.RoundToInt ((spacing - radius) / radius);
		if (nr_rep < 0)
			nr_rep = 0;
		//print (nr_rep);

		incrementz = spacing * scale;

		int i = 0;
		for (int z = 0; z < fatias; z++) {
			for(int rep = 0; rep <= nr_rep; rep++){
				for (int x = 0; x < largura; x = x + intincx) {
					for (int y = 0; y < altura; y = y + intincy) {
						
						Vector3 p = new Vector3(x, y, z);
						
						p.x = (p.x*resolution) * incrementx - scale/2f;
						p.y = (p.y*resolution) * incrementy - scale/2f;
						p.z = p.z * incrementz +(rep*(incrementz/(nr_rep+1))) - (fatias*incrementz/2f);
						
						points[i].position = p;
						
						points[i].size = radius;
						
						original_points[i] = points[i];
						i++;
						
					}
				}
			}
		}
		
		transform.localScale = new Vector3 (scale, scale, fatias * incrementz);
		
		GetComponent<ParticleSystem>().SetParticles(points, points.Length);
	}
}
