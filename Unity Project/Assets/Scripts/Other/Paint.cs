﻿using UnityEngine;
using System.Collections;

public class Paint : MonoBehaviour {

	// Variaveis da figura
	private Texture2D default_texture;
	private Texture2D nova_imagem;


	// Origem da funçao
	public CustomFunction fonte;

	// Funçao de tranferencia
	private float[] FT;

	void Start () {

		default_texture = (Texture2D)Instantiate(Resources.Load("blank")); 

		// Ou

		//default_texture = (Texture2D)Resources.Load ("blank");

	
	}


	void drawFunc (){

		//renderer.material.mainTexture = default_texture;

		FT = fonte.funcao_transferencia;

		nova_imagem = default_texture;

		for (int x=0; x<FT.Length; x++) {

			int FTint = Mathf.RoundToInt(FT[x]);

			nova_imagem.SetPixel(x, FTint, Color.blue);

		}
		nova_imagem.Apply();

		GetComponent<Renderer>().material.mainTexture = nova_imagem;

	}
}
