﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Touchscreen : MonoBehaviour 
{
	
	public float minSwipeDistY;
	
	public float minSwipeDistX;
	
	private Vector2 startPos;
	private Vector2 startPos_2;

	// Toggle Lock
	private bool unlocked = false;

	// Target
	public GameObject target;

	// Rotaçao de camera
	public RotateDown RD;
	public RotateLeft RL;
	public RotateRight RR;
	public RotateUp RU;

	//Sensibilidades
	public float sentivity = 1;
	public float fov_sensitivity = 1;
	public float trans_sensitivity = 1;

	// Raycasting para os cubos
	/*public LayerMask touchInputMask;
	
	private List<GameObject> touchList = new List<GameObject>();
	private GameObject[] touchesOld;

	private RaycastHit hit;*/
	
	void Update()
	{
		// Translaçao da camera - Mudar este codigo para outro script
		if (Input.GetKey ("a")) {
			target.transform.Translate (-Vector3.right * trans_sensitivity * Time.deltaTime);
			transform.Translate (-Vector3.right * trans_sensitivity * Time.deltaTime);
		}

		if (Input.GetKey ("d")) {
			target.transform.Translate (Vector3.right * trans_sensitivity * Time.deltaTime);
			transform.Translate (Vector3.right * trans_sensitivity * Time.deltaTime);
		}

		if (Input.GetKey ("w")) {
			target.transform.Translate (Vector3.up * trans_sensitivity * Time.deltaTime);
			transform.Translate (Vector3.up * trans_sensitivity * Time.deltaTime);
		}

		if (Input.GetKey ("s")) {
			target.transform.Translate (-Vector3.up * trans_sensitivity * Time.deltaTime);
			transform.Translate (-Vector3.up * trans_sensitivity * Time.deltaTime);
		}

		if (Input.GetKey ("f")) {
			transform.Translate (Vector3.forward * trans_sensitivity * Time.deltaTime);
		}

		if (Input.GetKey ("b")) {
			transform.Translate (-Vector3.forward * trans_sensitivity * Time.deltaTime);
		}

		// Verificar se e permitido usar controlos touch na camera

		if (unlocked) {
#if UNITY_EDITOR
		
			if (Input.GetMouseButton (0) || Input.GetMouseButtonUp (0) || Input.GetMouseButtonDown (0)) {
				// Codigo para ler os cubos


				// Funçoes de drag

				if (Input.GetMouseButtonDown (0)) {	
					startPos = Input.mousePosition;
				}

				if (Input.GetMouseButton (0)) {
				
					float swipeDistVertical = (new Vector3 (0, Input.mousePosition.y, 0) - new Vector3 (0, startPos.y, 0)).magnitude;
				
					if (swipeDistVertical > minSwipeDistY) {
						float swipeValue = Mathf.Sign (Input.mousePosition.y - startPos.y);
					
						if (swipeValue > 0) {//up swipe
						
							RD.RotateCameraDown (Mathf.Abs (swipeValue) * sentivity);
						} else {
							if (swipeValue < 0) {//down swipe
							
								RU.RotateCameraUp (Mathf.Abs (swipeValue) * sentivity);
							}
						}		
					}
				
					float swipeDistHorizontal = (new Vector3 (Input.mousePosition.x, 0, 0) - new Vector3 (startPos.x, 0, 0)).magnitude;
				
					if (swipeDistHorizontal > minSwipeDistX) {
						float swipeValue = Mathf.Sign (Input.mousePosition.x - startPos.x);
					
						if (swipeValue > 0) {//right swipe
						
							RL.RotateCameraLeft (Mathf.Abs (swipeValue) * sentivity);
						} else {
							if (swipeValue < 0) {//left swipe
							
								RR.RotateCameraRight (Mathf.Abs (swipeValue) * sentivity);
							}
						}	
					}
				
					startPos = Input.mousePosition;
				}
			}
#endif

			if (Input.touchCount > 0) {
				Touch touch = Input.touches [0];

				if (Input.touchCount > 1) {
					// Codigo para o zoom e a translaçao

					Touch touch_2 = Input.touches [1];

					switch (touch_2.phase) {
					case TouchPhase.Began:
						startPos = touch.position;
						startPos_2 = touch_2.position;
						break;

					case TouchPhase.Moved:
					// Ver distancia entre os dedos
					//float swipeDistVertical = (new Vector3(0, touch.position.y, 0) - new Vector3(0, startPos.y, 0)).magnitude;
					//float swipeDistHorizontal = (new Vector3(touch.position.x,0, 0) - new Vector3(startPos.x, 0, 0)).magnitude;

						float first_distance = Mathf.Abs (startPos.x - startPos_2.x);
						float second_distance = Mathf.Abs (touch.position.x - touch_2.position.x);

					// Zoom 
						float fov = GetComponent<Camera>().fieldOfView;
						fov = fov + (second_distance - first_distance) * fov_sensitivity;
						GetComponent<Camera>().fieldOfView = fov;

					// Translaçao 


						break;
					}
			
			
			
				}

				switch (touch.phase) {	
				case TouchPhase.Began:	
					startPos = touch.position;
					break;
				
				case TouchPhase.Moved:
				
					float swipeDistVertical = (new Vector3 (0, touch.position.y, 0) - new Vector3 (0, startPos.y, 0)).magnitude;

					if (swipeDistVertical > minSwipeDistY) {
						float swipeValue = Mathf.Sign (touch.position.y - startPos.y);

						if (swipeValue > 0) {//up swipe
						
							RD.RotateCameraDown (Mathf.Abs (swipeValue) * sentivity);
						} else {
							if (swipeValue < 0) {//down swipe

								RU.RotateCameraUp (Mathf.Abs (swipeValue) * sentivity);
							}
						}		
					}
				
					float swipeDistHorizontal = (new Vector3 (touch.position.x, 0, 0) - new Vector3 (startPos.x, 0, 0)).magnitude;
				
					if (swipeDistHorizontal > minSwipeDistX) {
						float swipeValue = Mathf.Sign (touch.position.x - startPos.x);
					
						if (swipeValue > 0) {//right swipe
						
							RL.RotateCameraLeft (Mathf.Abs (swipeValue) * sentivity);
						} else {
							if (swipeValue < 0) {//left swipe
							
								RR.RotateCameraRight (Mathf.Abs (swipeValue) * sentivity);
							}
						}	
					}

					startPos = touch.position;
					break;
				}
			}
		}
	}

	// Esta funçao bloqueia/desbloqueia os controlos de touch do ecra quando desenhamos a funçao de trasnferencia
	public void Toggle_Lock (){
		unlocked = !unlocked;
		//print ("lock toggled");
	}
}