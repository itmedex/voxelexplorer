﻿using UnityEngine;
using System.Collections;

public class Draganddrop_ID : MonoBehaviour {

	Camera touchcamera;
	public Camera usecamera;
	private bool _mouseState;
	public GameObject Target;
	public Vector3 screenSpace;
	public Vector3 offset;

	// Origem dos limites
	public Cursor cursor;
	private Vector3 initialcoor;
	private Vector3 finalcoor;

	// Dois cilindros de referencia
	public GameObject SD;
	public GameObject IE;

	// Use this for initialization
	void Start ()
	{
		touchcamera = usecamera;
	}
	
	// Update is called once per frame
	void Update ()
	{
		CheckPos ();

		// Debug.Log(_mouseState);
		if (Input.GetMouseButtonDown (0)) {
			RaycastHit hitInfo;
			if (Target == GetClickedObject (out hitInfo)) {
				_mouseState = true;
				screenSpace = touchcamera.WorldToScreenPoint (Target.transform.position);
				offset = Target.transform.position - touchcamera.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, screenSpace.z));
			}
		}
		if (Input.GetMouseButtonUp (0)) {
			_mouseState = false;
		}
		if (_mouseState) {
			//keep track of the mouse position
			var curScreenSpace = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, screenSpace.z);
			
			//convert the screen mouse position to world point and adjust with offset
			var curPosition = touchcamera.ScreenToWorldPoint (curScreenSpace) + offset;
			
			//update the position of the object in the world
			Target.transform.position = curPosition;
		}
	}
	
	
	GameObject GetClickedObject (out RaycastHit hit)
	{
		GameObject target = null;
		Ray ray = touchcamera.ScreenPointToRay (Input.mousePosition);
		if (Physics.Raycast (ray.origin, ray.direction * 10, out hit)) {
			target = hit.collider.gameObject;
		}
		
		return target;
	}

	void CheckPos(){

		Vector3 newpos = transform.position;
		
		if (transform.position.x > cursor.finalcoor.x) {
			newpos.x = cursor.finalcoor.x;
		}
		
		if (transform.position.z < cursor.initialcoor.z) {
			newpos.z = cursor.initialcoor.z;
		}
		if (transform.position.x < IE.transform.position.x) {
			newpos.x = IE.transform.position.x;
		}
		if (transform.position.z > SD.transform.position.z) {
			newpos.z = SD.transform.position.z;
		}
		
		if (newpos != transform.position) {
			transform.position = newpos;
		}

	}
}
