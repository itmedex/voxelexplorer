﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SelectColor : MonoBehaviour {

	// Cor a ser aplicada
	private Color selected_color;

	// Visualizar cor selccionada
	public GameObject tela;
	private Image color_display;

	// Lista de tecidos seleccionados
	public int[] tecidos_activados;
	public Color[] cores_tecidos;

	// Visualizar cor de cada tecido
	public GameObject lung_color;
	public GameObject fat_color;
	public GameObject water_color;
	public GameObject kidney_color;
	public GameObject blood_color;
	public GameObject muscle_color;
	public GameObject greym_color;
	public GameObject whitem_color;
	public GameObject liver_color;
	public GameObject bone_color;

	// Actualiza a funçao de cores
	public ColorFunc2 color_func;

	// Use this for initialization
	void Start () {
		selected_color = new Color (1, 1, 1, 1);
		tecidos_activados = new int[10];
		cores_tecidos = new Color[10];
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// Selecçao de cores
	public void Select_Black (){
		selected_color = Color.black;
		color_display = tela.GetComponent<Image> ();
		color_display.color = Color.black;
	}

	public void Select_Blue (){
		selected_color = Color.blue;
		color_display = tela.GetComponent<Image> ();
		color_display.color = Color.blue;
	}

	public void Select_Cyan (){
		selected_color = Color.cyan;
		color_display = tela.GetComponent<Image> ();
		color_display.color = Color.cyan;
	}

	public void Select_Gray (){
		selected_color = Color.gray;
		color_display = tela.GetComponent<Image> ();
		color_display.color = Color.gray;
	}

	public void Select_Green (){
		selected_color = Color.green;
		color_display = tela.GetComponent<Image> ();
		color_display.color = Color.green;
	}

	public void Select_Magenta (){
		selected_color = Color.magenta;
		color_display = tela.GetComponent<Image> ();
		color_display.color = Color.magenta;
	}

	public void Select_Red (){
		selected_color = Color.red;
		color_display = tela.GetComponent<Image> ();
		color_display.color = Color.red;
	}

	public void Select_White(){
		selected_color = Color.white;
		color_display = tela.GetComponent<Image> ();
		color_display.color = Color.white;
	}

	public void Select_Yellow (){
		selected_color = Color.yellow;
		color_display = tela.GetComponent<Image> ();
		color_display.color = Color.yellow;
	}

	// Atribuir cores a tecidos
	public void Set_Lung (){
		color_display = lung_color.GetComponent<Image> ();
		color_display.color = selected_color;
		cores_tecidos [0] = selected_color;
	}
	public void Set_Fat (){
		color_display = fat_color.GetComponent<Image> ();
		color_display.color = selected_color;
		cores_tecidos [1] = selected_color;
	}
	public void Set_Water (){
		color_display = water_color.GetComponent<Image> ();
		color_display.color = selected_color;
		cores_tecidos [2] = selected_color;
	}
	public void Set_Kidney (){
		color_display = kidney_color.GetComponent<Image> ();
		color_display.color = selected_color;
		cores_tecidos [3] = selected_color;
	}
	public void Set_Blood (){
		color_display = blood_color.GetComponent<Image> ();
		color_display.color = selected_color;
		cores_tecidos [4] = selected_color;
	}
	public void Set_Muscle (){
		color_display = muscle_color.GetComponent<Image> ();
		color_display.color = selected_color;
		cores_tecidos [5] = selected_color;
	}
	public void Set_GreyM (){
		color_display = greym_color.GetComponent<Image> ();
		color_display.color = selected_color;
		cores_tecidos [6] = selected_color;
	}
	public void Set_WhiteM (){
		color_display = whitem_color.GetComponent<Image> ();
		color_display.color = selected_color;
		cores_tecidos [7] = selected_color;
	}
	public void Set_Liver (){
		color_display = liver_color.GetComponent<Image> ();
		color_display.color = selected_color;
		cores_tecidos [8] = selected_color;
	}
	public void Set_Bone (){
		color_display = bone_color.GetComponent<Image> ();
		color_display.color = selected_color;
		cores_tecidos [9] = selected_color;
	}

	// Seleccionar tecidos

	public void Select_Lung (){
		if (tecidos_activados [0] == 0) {
			tecidos_activados [0] = 1;
		}
		else{
			tecidos_activados [0] = 0;
		}
	}

	public void Select_Fat (){
		if (tecidos_activados [1] == 0) {
			tecidos_activados [1] = 1;
		}
		else{
			tecidos_activados [1] = 0;
		}
	}

	public void Select_Water (){
		if (tecidos_activados [2] == 0) {
			tecidos_activados [2] = 1;
		}
		else{
			tecidos_activados [2] = 0;
		}
	}

	public void Select_Kidney (){
		if (tecidos_activados [3] == 0) {
			tecidos_activados [3] = 1;
		}
		else{
			tecidos_activados [3] = 0;
		}
	}

	public void Select_Blood (){
		if (tecidos_activados [4] == 0) {
			tecidos_activados [4] = 1;
		}
		else{
			tecidos_activados [4] = 0;
		}
	}

	public void Select_Muscle (){
		if (tecidos_activados [5] == 0) {
			tecidos_activados [5] = 1;
		}
		else{
			tecidos_activados [5] = 0;
		}
	}
	
	public void Select_GreyM (){
		if (tecidos_activados [6] == 0) {
			tecidos_activados [6] = 1;
		}
		else{
			tecidos_activados [6] = 0;
		}
	}
	
	public void Select_WhiteM (){
		if (tecidos_activados [7] == 0) {
			tecidos_activados [7] = 1;
		}
		else{
			tecidos_activados [7] = 0;
		}
	}
	
	public void Select_Liver (){
		if (tecidos_activados [8] == 0) {
			tecidos_activados [8] = 1;
		}
		else{
			tecidos_activados [8] = 0;
		}
	}
	
	public void Select_Bone(){
		if (tecidos_activados [9] == 0) {
			tecidos_activados [9] = 1;
		}
		else{
			tecidos_activados [9] = 0;
		}
	}
	// Submeter dados para o programa
	public void Submit(){
		color_func.SendMessage("Link_Color", SendMessageOptions.DontRequireReceiver);

	}


}
