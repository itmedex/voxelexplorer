﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SelectScene : MonoBehaviour {

	public Cursor Free_form;
	public Touchscript free_draw;
	public ExampleClass hist_select;
	public GameObject Ref_shape;
	public GameObject Square_shape;
	public GameObject Line_Ref;
	public GameObject Hist_Plane;
	public GameObject color_menu;
	public GameObject View_Canvas;
	public GameObject TouchCamera;

	// Avisar qual o tipo de funçao a usar
	public GameObject display_text;
	Text instruction;


	void Start () {

		instruction = display_text.GetComponent<Text>();
		instruction.text = " ";

		Free_1D ();

	}

	public void Free_1D (){

		// Posso mexer o cursor
		free_draw.unlocked = true;
		// Funçao de 1D
		Free_form.One_dim = true;
		// Coloca o plano no local certo
		Vector3 pos = Hist_Plane.transform.position;
		pos.y = -2f;
		Hist_Plane.transform.position = pos;
		// Desenhar ou apagar o histograma 2D
		hist_select.Reset_blank ();
		// Desactivar ou desactivar elementos de interesse
		Ref_shape.SetActive(false);
		Line_Ref.SetActive(false);
		Square_shape.SetActive(false);
		// Aviso de funçao
		instruction.text = " Free 1D Function";



	}

	public void Ref_1D () {
		// Posso mexer o cursor
		free_draw.unlocked = false;
		// Funçao de 1D
		Free_form.One_dim = true;
		// Coloca o plano no local certo
		Vector3 pos = Hist_Plane.transform.position;
		pos.y = -2f;
		Hist_Plane.transform.position = pos;
		// Desenhar ou apagar o histograma 2D
		hist_select.Reset_blank ();
		// Desactivar ou desactivar elementos de interesse
		Ref_shape.SetActive(false);
		Line_Ref.SetActive(true);
		Square_shape.SetActive(false);
		// Aviso de funçao
		instruction.text = " Referenced 1D Function";

	}

	public void Free_2D () {
		// Posso mexer o cursor
		free_draw.unlocked = true;
		// Funçao de 1D
		Free_form.One_dim = false;
		// Coloca o plano no local certo
		Vector3 pos = Hist_Plane.transform.position;
		pos.y = -1f;
		Hist_Plane.transform.position = pos;
		// Desenhar ou apagar o histograma 2D
		hist_select.draw_hist2d ();
		// Desactivar ou desactivar elementos de interesse
		Ref_shape.SetActive(false);
		Line_Ref.SetActive(false);
		Square_shape.SetActive(false);
		// Aviso de funçao
		instruction.text = " Free 2D Function";
	}

	public void Ref_2D (){
		// Posso mexer o cursor
		free_draw.unlocked = false;
		// Funçao de 1D
		Free_form.One_dim = false;
		// Coloca o plano no local certo
		Vector3 pos = Hist_Plane.transform.position;
		pos.y = -1f;
		Hist_Plane.transform.position = pos;
		// Desenhar ou apagar o histograma 2D
		hist_select.draw_hist2d ();
		// Desactivar ou desactivar elementos de interesse
		Ref_shape.SetActive(true);
		Line_Ref.SetActive(false);
		Square_shape.SetActive(false);
		// Aviso de funçao
		instruction.text = " Referenced 2D Function";
	}

	public void Square_2D (){
		// Posso mexer o cursor
		free_draw.unlocked = false;
		// Funçao de 1D
		Free_form.One_dim = false;
		// Coloca o plano no local certo
		Vector3 pos = Hist_Plane.transform.position;
		pos.y = -1f;
		Hist_Plane.transform.position = pos;
		// Desenhar ou apagar o histograma 2D
		hist_select.draw_hist2d ();
		// Desactivar ou desactivar elementos de interesse
		Ref_shape.SetActive(false);
		Line_Ref.SetActive(false);
		Square_shape.SetActive(true);
		// Aviso de funçao
		instruction.text = " Square 2D Function";
	}

	public void Color_Select(){
		color_menu.SetActive (true);
		View_Canvas.SetActive (false);
		TouchCamera.SetActive (false);
	}

	public void Color_Submit(){
		color_menu.SetActive (false);
		View_Canvas.SetActive (true);
		TouchCamera.SetActive (true);
	}

}
