﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Plane_pos : MonoBehaviour {



	public Slider height;

	public Slider rotatex;

	public Slider rotatey;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		Vector3 pos = transform.position;

		pos.y = height.value - 0.5f;

		transform.position = pos;

		float anglex = (rotatex.value * 180f) - 90f;
		float angley = (rotatey.value * 180f) - 90f;

		transform.eulerAngles = new Vector3 (anglex, 0, angley);
	}
}
