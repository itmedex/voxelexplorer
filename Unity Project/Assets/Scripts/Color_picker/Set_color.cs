﻿using UnityEngine;
using System.Collections;

public class Set_color : MonoBehaviour {

	public GameObject thumb_hue;
	public GameObject thumb_bs;

	void Set_cursors(Color color){
		//print ("got message");

		HSBColor new_color = HSBColor.FromColor (color);

		float hue = new_color.h;
		float sat = new_color.s;
		float brg = new_color.b;

		float y_pos_hue = 100 - hue * 200;

		float x_pos_brg = 45 - brg * 90;

		float y_pos_sat = 45 - sat * 90;

		Vector2 b_s = new Vector2 (x_pos_brg, y_pos_sat);

		thumb_hue.SendMessage ("Set_custom_H", y_pos_hue);
		thumb_bs.SendMessage ("Set_custom_BS", b_s);
	}
}
