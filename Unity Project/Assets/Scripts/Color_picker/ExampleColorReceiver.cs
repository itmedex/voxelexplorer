using UnityEngine;

public class ExampleColorReceiver : MonoBehaviour {
	
    Color color;
	public GameObject color_camera;
	private Camera camera;
	
	void Start ()
	{
		
		camera = color_camera.GetComponent<Camera> (); // vai buscar a componente camera da camera color
		
		
	}

	void OnColorChange(HSBColor color) 
	{
        this.color = color.ToColor();
	}

    void OnGUI()
    {
		var r = camera.pixelRect;
		var rect = new Rect(r.center.x + r.height / 6 + 50, r.center.y, 100, 100);
		GUI.Label (rect, "#" + ToHex(color.r) + ToHex(color.g) + ToHex(color.b));	
    }

	string ToHex(float n)
	{
		return ((int)(n * 255)).ToString("X").PadLeft(2, '0');
	}
}
