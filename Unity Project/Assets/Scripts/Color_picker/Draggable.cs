using UnityEngine;

public class Draggable : MonoBehaviour
{
	public bool fixX;
	public bool fixY;
	public Transform thumb;	
	bool dragging;
	public GameObject color_camera;
	private Camera camera;

	public Manipulateobject screen;
	public Function_draw draw;

	void Start ()
	{

		camera = color_camera.GetComponent<Camera> (); // vai buscar a componente camera da camera color


	}
	void Update()
	{
		if (TuioInput.touchCount > 0) 
		{
			Touch touch= TuioInput.touches[0]; 

			if (touch.phase == TouchPhase.Began) {
				dragging = false;
				var ray = camera.ScreenPointToRay(touch.position);
				RaycastHit hit;
				if (GetComponent<Collider>().Raycast(ray, out hit, 100)) {
					dragging = true;
				}
			}
			if (touch.phase == TouchPhase.Ended)
			{ 
				dragging = false;
			}

			if (dragging && touch.phase == TouchPhase.Moved) 
			{
				var point = camera.ScreenToWorldPoint(touch.position);
				point = GetComponent<Collider>().ClosestPointOnBounds(point);
				SetThumbPosition(point);
				SendMessage("OnDrag", Vector3.one - (thumb.position - GetComponent<Collider>().bounds.min) / GetComponent<Collider>().bounds.size.y);//mudei de x
			}
		}
	}

	void SetDragPoint(Vector3 point)
	{
		point = (Vector3.one - point) * GetComponent<Collider>().bounds.size.x + GetComponent<Collider>().bounds.min;
		SetThumbPosition(point);
	}

	void SetThumbPosition(Vector3 point)
	{
		thumb.position = new Vector3(fixX ? thumb.position.x : point.x, fixY ? thumb.position.y : point.y, thumb.position.z);
	}

	void Set_custom_H(float hue_pos){

		Vector3 pos = thumb.localPosition;
		pos.y = hue_pos;
		thumb.localPosition = pos;
		SendMessage("OnDrag", Vector3.one - (thumb.position - GetComponent<Collider>().bounds.min) / GetComponent<Collider>().bounds.size.y);//mudei de x
	}

	void Set_custom_BS(Vector2 b_s){

		Vector3 pos = thumb.localPosition;
		pos.x = b_s.x;
		pos.y = b_s.y;
		thumb.localPosition = pos;
		SendMessage("OnDrag", Vector3.one - (thumb.position - GetComponent<Collider>().bounds.min) / GetComponent<Collider>().bounds.size.y);//mudei de x
	}
}

/*	if (Input.GetMouseButtonDown(0)) {
			dragging = false;
			var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (collider.Raycast(ray, out hit, 100)) {
				dragging = true;
			}
		}
		if (Input.GetMouseButtonUp(0)) dragging = false;
		if (dragging && Input.GetMouseButton(0)) {
			var point = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			point = collider.ClosestPointOnBounds(point);
			SetThumbPosition(point);
			SendMessage("OnDrag", Vector3.one - (thumb.position - collider.bounds.min) / collider.bounds.size.x);
		}
 */