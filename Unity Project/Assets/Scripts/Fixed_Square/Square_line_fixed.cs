﻿using UnityEngine;
using System.Collections;

public class Square_line_fixed : MonoBehaviour {


	// Line Variables
	public GameObject lines;
	private LineRenderer path;
	private int i;

	// Vertices
	public GameObject SE;
	public GameObject SD;
	public GameObject IE;
	public GameObject ID;

	// Origem das medidas do quadro
	public Cursor cursor;

	// Matrix de periferia
	public int[,] perif_mat;

	// Manda preencher a matriz
	public Mapping mapa;

	// Projecta para o plano para testar - retirar quando funcionar bem
	public ExampleClass plano;

	void Start () {

		i = 5;

		path = lines.GetComponent<LineRenderer> ();
		path.SetVertexCount (i);
		path.SetPosition(0, SE.transform.position);
		path.SetPosition(1, SD.transform.position);
		path.SetPosition(2, ID.transform.position);
		path.SetPosition(3, IE.transform.position);
		path.SetPosition(4, SE.transform.position);
	
	}

	void Update () {

		path.SetPosition(0, SE.transform.position);
		path.SetPosition(1, SD.transform.position);
		path.SetPosition(2, ID.transform.position);
		path.SetPosition(3, IE.transform.position);
		path.SetPosition(4, SE.transform.position);
	
		Make_perif ();

		//plano.SendMessage ("draw_perif", SendMessageOptions.DontRequireReceiver);
		mapa.SendMessage ("mapear_square_fixo", SendMessageOptions.DontRequireReceiver);


	}

	void Make_perif(){

		///// Criar a matrix

		perif_mat = new int[256, 101];

		///// Centrar e escalar os quatro pontos

		// Centrar as coordenadas dos pontos
		//SE
		float xcent_SE = SE.transform.position.x - cursor.initialcoor.x;
		float ycent_SE = SE.transform.position.z - cursor.initialcoor.z;
		//ID
		float xcent_ID = ID.transform.position.x - cursor.initialcoor.x;
		float ycent_ID = ID.transform.position.z - cursor.initialcoor.z;

		// Escalar valores de x
		float xescal_SE = (xcent_SE / cursor.plane_lenght) * 255;
		float xescal_ID = (xcent_ID / cursor.plane_lenght) * 255;
		// Valor escalado inteiro de x
		int xescalint_SE = Mathf.RoundToInt (xescal_SE);
		int xescalint_ID = Mathf.RoundToInt (xescal_ID);
		
		// Escalar valores de x
		float yescal_SE = (ycent_SE / cursor.plane_height) * 100;
		float yescal_ID = (ycent_ID / cursor.plane_height) * 100;

		// Valor escalado inteiro de x
		int yescalint_SE = Mathf.RoundToInt (yescal_SE);
		int yescalint_ID = Mathf.RoundToInt (yescal_ID);

		///// Definir os pontos na matrix
		perif_mat [xescalint_SE, yescalint_SE] = 1;
		perif_mat [xescalint_ID, yescalint_ID] = 1;

		for (int x = xescalint_SE; x <= xescalint_ID; x++) {

			perif_mat [x, yescalint_SE] = 1;
			perif_mat [x, yescalint_ID] = 1;

		}

		for (int y = yescalint_ID; y <= yescalint_SE; y++) {

			perif_mat [xescalint_ID, y] = 1;
			perif_mat [xescalint_SE, y] = 1;

		}

	}
}
	