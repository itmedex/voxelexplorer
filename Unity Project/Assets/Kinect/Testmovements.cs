﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Testmovements : MonoBehaviour {


	private Vector3 headPos;
	
	private Vector3 leftHandPos;
	private Vector3 leftWristPos;
	private Vector3 leftElbowPos;
	private Vector3 leftShoulderPos;
	
	private Vector3 rightHandPos;
	private Vector3 rightWristPos;
	private Vector3 rightElbowPos;
	private Vector3 rightShoulderPos;

	private Vector3 leftThumbPos;
	private Vector3 rightThumbPos;
	private Vector3 leftHandTipPos;
	private Vector3 rightHandTipPos;

	private string lHandState;
	private string rHandState;

	// Receptor dos dados do Kinect
	public UDPReceive receiver;

	// Palmas

	public float clap_thresh = 1f;

	// Cubos
	public GameObject cube;
	//public Grapher5Up target_system;

	// Camera
	public GameObject camera_object;
	public Camera camera;
	public Canvas canvas;
	public GameObject panel_R;
	private Image[] right_buttons;
	public Image button_R_1;
	public Image button_R_2;
	public Image button_R_3;
	public Image button_R_4;
	public GameObject panel_L;
	private Image[] left_buttons;
	public Image button_L_1;
	public Image button_L_2;
	public Image button_L_3;
	public Image button_L_4;

	// Tipo de funçao a ser usado
	private string funcao;
	public Text funcao_display;
	public Text axis_text;

	// Variaveis para definir o ecra
	public float screen_proportion = 1.5f;
	private Vector3 initialcoor;
	private Vector3 finalcoor;
	private float screen_wide;
	private float screen_high;

	// Teste de rotaçao
	private Quaternion initialrotation;
	private Vector3 offset_left;
	private Vector3 last_left;


	// Teste de traslaçao
	private bool held;

	private Vector3 initialposition;
	private float last_held;
	
	// Criar um intervalo de bloqueio
	// Com este intervalo nao havera mudanças acidentais do modelo apos manipulaçao
	private float block_period;

	// Teste de zoom
	private bool zooming;
	private float last_distance;

	// Tag
	public GameObject pointer;
	private GameObject first_marker;

	// Angle
	private GameObject angle_1;
	private GameObject angle_2;
	private GameObject angle_3;
	private GameObject angle_line_1;

	// Plano
	public GameObject Clipping_plane;
	public GameObject P1;
	public GameObject P2;
	public GameObject P3;

	// Clip box
	private Vector3 target_scale;

	//
	public GameObject Target;
	private GameObject func_Tatget;
	private string movement_type;
	private bool holding;
	private bool draging;

	// Scaling
	private float scale_offset;
	private float scale;

	// Reset Time
	private float reset_time;

	//
	public GameObject marker_1;
	public GameObject marker_2;

	//
	private float button_block;
	private float rotate_reset;

	// 
	public bool drag_rotate;

	//
	public float screen_length_real;
	public float screen_height_real;
	private float min_scale = 1f;
	private float max_scale = 10f;

	//Scale
	public GameObject scale_bar;
	public TextMesh scale_label;

	// Widget Camera
	public Camera widget_camera;

	//Frame
	public GameObject frame;

	// Use this for initialization
	void Start () {
	
		initialrotation = cube.transform.localRotation;
		initialposition = cube.transform.position;

		scale = 1f;
		zooming = false;
		movement_type = "";
		funcao = "";
		funcao_display.text = funcao;

		angle_1 = null;
		angle_2 = null;
		angle_3 = null;


		right_buttons = new Image[4];
		right_buttons [3] = button_R_1;
		right_buttons [2] = button_R_2;
		right_buttons [1] = button_R_3;
		right_buttons [0] = button_R_4;

		left_buttons = new Image[4];
		left_buttons [3] = button_L_1;
		left_buttons [2] = button_L_2;
		left_buttons [1] = button_L_3;
		left_buttons [0] = button_L_4;
	}
	
	// Update is called once per frame
	void Update () {
	
		// Actualizaçao das coordenadas

		headPos = receiver.headPos;
		
		leftHandPos = receiver.leftHandPos;
		leftWristPos = receiver.leftWristPos;
		leftElbowPos = receiver.leftElbowPos;
		leftShoulderPos = receiver.leftShoulderPos;
		
		rightHandPos = receiver.rightHandPos;
		rightWristPos = receiver.rightWristPos;
		rightElbowPos = receiver.rightElbowPos;
		rightShoulderPos = receiver.rightShoulderPos;

		leftThumbPos = receiver.leftThumbPos;
		rightThumbPos = receiver.rightThumbPos;
		leftHandTipPos = receiver.leftHandTipPos;
		rightHandTipPos = receiver.rightHandTipPos;
		
		lHandState = receiver.lHandState;
		rHandState = receiver.rHandState;
		//

		axis_text.text = movement_type;
		// Define os limites do espaço de interacçao

		Define_screen ();

		GameObject[] last_dot = GameObject.FindGameObjectsWithTag ("touched");
		for (int i= 0; i<last_dot.Length; i++) {
			Destroy (last_dot [i]);
		}


		Tag_direction ();

		// Criar cursores no ecra para mostrar a posiçao das maos
		Vector2 p_l = Screen_positions (leftHandPos); 
		Vector2 p_r = Screen_positions (rightHandPos);


		if (Within_screen (leftHandPos)) {
			//  instanciar e posicionar um ponto no ecra (visual feedback)
			GameObject dot = (GameObject)Instantiate (Resources.Load ("touched"));
			dot.transform.SetParent (canvas.transform, false);
			Vector3 dotpos = new Vector3 (p_l.x, p_l.y, 10f);
			dot.transform.position = dotpos;
		}

		if (Within_screen (rightHandPos)) {
			GameObject dot2 = (GameObject)Instantiate (Resources.Load ("touched"));
			dot2.transform.SetParent (canvas.transform, false);
			Vector3 dotpos2 = new Vector3 (p_r.x, p_r.y, 10f);
			dot2.transform.position = dotpos2;
		}


		if (funcao == "") {

			/*
			if (rHandState == "closed" && lHandState == "closed" && Within_screen (leftHandPos) && Within_screen (rightHandPos)) {
				
				if (!zooming) {
					zooming = true;
					float last_distance = Vector3.Magnitude (leftHandPos - rightHandPos);
				} else {
					float dist = Vector3.Magnitude (leftHandPos - rightHandPos);
					float change = dist - last_distance;
					camera.fieldOfView = camera.fieldOfView - 30f * change;
					last_distance = dist;
				}
			} else {
				zooming = false;
			}
			*/



			if (zooming == false && rHandState == "closed" && lHandState == "closed" && Within_screen (leftHandPos) && Within_screen (rightHandPos)) {
				zooming=true;
				last_distance = Vector3.Magnitude (leftHandPos - rightHandPos);
			}

			if (zooming == true && rHandState == "open" && lHandState == "open") {
				zooming=false;
			}


			if(zooming){
				float dist = Vector3.Magnitude (leftHandPos - rightHandPos);
				float change = dist - last_distance;
				camera.fieldOfView = camera.fieldOfView - 30f * change;
				camera.orthographicSize = camera.orthographicSize - 30f * change;
				last_distance = dist;
			}
			/*
			// Limitar o field of view da camera
			if (camera.fieldOfView > 110f)
				camera.fieldOfView = 110f;
			if (camera.fieldOfView < 10f)
				camera.fieldOfView = 10f;
			*/
			/*
			if(zooming){
				float dist = Vector3.Magnitude (leftHandPos - rightHandPos);
				float change = dist - last_distance;
				Grapher5Up grafer = Target.GetComponent<Grapher5Up> ();
				scale = grafer.scale;
				scale = scale + change;
				if(scale < min_scale) scale = min_scale;
				if(scale > max_scale) scale = max_scale;
				Target.SendMessage("New_scale", scale, SendMessageOptions.DontRequireReceiver);
				Set_measure_bar();
				last_distance = dist;
			}*/

			if (rHandState == "lasso" && Within_screen (rightHandPos) && !zooming) {
				
				movement_type = "";
				Target = null;
			}


		}
	
		if (funcao == "Tag") {

			//print(func_Tatget);

			if (rHandState == "closed" && Within_screen(rightHandPos) && func_Tatget != null && Time.time > button_block){

				//print ("agarrou");
				if(first_marker == null){

					GameObject prov_marker = (GameObject)Instantiate (Resources.Load ("Kinect_prefabs/Pos_Marker"));
					prov_marker.transform.position = pointer.transform.position;
					first_marker = prov_marker;
					first_marker.transform.parent = func_Tatget.transform;

				}else{


					GameObject prov_marker = (GameObject)Instantiate (Resources.Load ("Kinect_prefabs/Pos_Marker"));
					prov_marker.transform.position = pointer.transform.position;
					prov_marker.transform.parent = func_Tatget.transform;

					GameObject measure_line = (GameObject)Instantiate (Resources.Load ("Kinect_prefabs/Tag_line"));

					measure_line.transform.position = (first_marker.transform.position+prov_marker.transform.position)*(0.5f);
					measure_line.transform.LookAt(prov_marker.transform);
					measure_line.transform.Rotate(new Vector3(90f, 0f,0f), Space.Self);
					measure_line.transform.parent = func_Tatget.transform;

					TextMesh measure = measure_line.GetComponentInChildren<TextMesh>();
					Vector3 linha = prov_marker.transform.position - first_marker.transform.position;
					float medida = linha.magnitude; // Adicionar medida da unidade
					measure_line.transform.localScale = new Vector3(0.05f, medida/2f, 0.05f);
					measure.text = medida.ToString();

					first_marker = null;
				}

				button_block = Time.time + 0.5f;
			}
		}

		if (funcao == "angle") {

			if (rHandState == "closed" && Within_screen(rightHandPos) && func_Tatget != null && Time.time > button_block){

				string option = "";
				if(angle_1 != null && angle_2 != null && angle_3 == null)option = "3";
				if(angle_1 != null && angle_2 == null && angle_3 == null)option = "2";
				if(angle_1 == null && angle_2 == null && angle_3 == null)option = "1";


				if(option == "3"){
					
					GameObject prov_marker = (GameObject)Instantiate (Resources.Load ("Kinect_prefabs/Pos_Marker"));
					prov_marker.transform.position = pointer.transform.position;
					angle_3 = prov_marker;
					angle_3.transform.parent = func_Tatget.transform;

					GameObject measure_line = (GameObject)Instantiate (Resources.Load ("Kinect_prefabs/angle_line"));
					
					measure_line.transform.position = (angle_2.transform.position+prov_marker.transform.position)*(0.5f);
					measure_line.transform.LookAt(prov_marker.transform);
					measure_line.transform.Rotate(new Vector3(90f, 0f,0f), Space.Self);
					measure_line.transform.parent = func_Tatget.transform;
					
					Vector3 linha = prov_marker.transform.position - angle_2.transform.position;
					float medida = linha.magnitude; // Adicionar medida da unidade
					measure_line.transform.localScale = new Vector3(0.05f, medida/2f, 0.05f);

					TextMesh angletext = angle_2.GetComponentInChildren<TextMesh>();
					Vector3 v1 = angle_1.transform.position - angle_2.transform.position;
					Vector3 v2 = angle_3.transform.position - angle_2.transform.position;
					float angle = Vector3.Angle(v1,v2);
					angle = angle * 100f;
					angle = Mathf.Round(angle);
					angle = angle / 100f;
					angletext.text = angle.ToString();

					angle_1 = null;
					angle_2 = null;
					angle_3 = null;
					angle_line_1 = null;
				}

				if(option == "2"){
					
					GameObject prov_marker = (GameObject)Instantiate (Resources.Load ("Kinect_prefabs/angle_marker"));
					prov_marker.transform.position = pointer.transform.position;
					angle_2 = prov_marker;
					angle_2.transform.parent = func_Tatget.transform;
					
					GameObject measure_line = (GameObject)Instantiate (Resources.Load ("Kinect_prefabs/angle_line"));
					angle_line_1 = measure_line;
					measure_line.transform.position = (angle_1.transform.position+prov_marker.transform.position)*(0.5f);
					measure_line.transform.LookAt(prov_marker.transform);
					measure_line.transform.Rotate(new Vector3(90f, 0f,0f), Space.Self);
					measure_line.transform.parent = func_Tatget.transform;
					
					Vector3 linha = angle_2.transform.position - angle_1.transform.position;
					float medida = linha.magnitude; // Adicionar medida da unidade
					measure_line.transform.localScale = new Vector3(0.05f, medida/2f, 0.05f);
					
				}

				if(option == "1"){
					
					GameObject prov_marker = (GameObject)Instantiate (Resources.Load ("Kinect_prefabs/Pos_Marker"));
					prov_marker.transform.position = pointer.transform.position;
					angle_1 = prov_marker;
					angle_1.transform.parent = func_Tatget.transform;
					
				}

				button_block = Time.time + 0.5f;
			}

		}

		if (funcao == "Clipping Plane") {


			if(Out_of_bouns_U(leftHandPos) && movement_type=="" ){
				Quaternion rot = Target.transform.rotation;
				rot = initialrotation;
				Target.transform.rotation = rot;
			}

			if(Out_of_bouns_B(leftHandPos) && movement_type=="" ){
				Quaternion rot = Target.transform.rotation;
				rot = initialrotation;
				rot.x = 180f;
				Target.transform.rotation = rot;
			}

			if (rHandState == "closed" && Within_screen(rightHandPos) && func_Tatget != null){


				Vector3[] plane_points = new Vector3[3];
				plane_points[0] = P1.transform.position;
				plane_points[1] = P2.transform.position;
				plane_points[2] = P3.transform.position;

				//print (plane_points[0] + " " + plane_points[1] + " " + plane_points[1]);
				func_Tatget.SendMessage("Clip_plane", plane_points,SendMessageOptions.DontRequireReceiver);
			}

		}

		if (funcao == "Clipping Box") {


			Target.transform.rotation = func_Tatget.transform.rotation;
			
			target_scale.x = func_Tatget.transform.localScale.x;
			target_scale.y = func_Tatget.transform.localScale.y;
			target_scale.z = func_Tatget.transform.localScale.z;
			
			pointer.transform.parent = func_Tatget.transform;
			
			Vector3 new_pointer_pos = pointer.transform.localPosition;

			if (pointer.transform.localPosition.x > target_scale.x/2f) new_pointer_pos.x = target_scale.x/2f;
			if (pointer.transform.localPosition.x < -target_scale.x/2f) new_pointer_pos.x = -target_scale.x/2f;
			if (pointer.transform.localPosition.y > target_scale.y/2f) new_pointer_pos.y = target_scale.y/2f;
			if (pointer.transform.localPosition.y < -target_scale.y/2f) new_pointer_pos.y = -target_scale.y/2f;
			if (pointer.transform.localPosition.z > target_scale.z/2f) new_pointer_pos.z = target_scale.z/2f;
			if (pointer.transform.localPosition.z < -target_scale.z/2f) new_pointer_pos.z = -target_scale.z/2f;
			/*
			if (pointer.transform.localPosition != new_pointer_pos){
				pointer.transform.localPosition = new_pointer_pos;
				func_Tatget.SendMessage("Clip_box", new_pointer_pos, SendMessageOptions.DontRequireReceiver);
			}
			*/
			pointer.transform.localPosition = new_pointer_pos;
			func_Tatget.SendMessage("Clip_box", pointer.transform.localPosition, SendMessageOptions.DontRequireReceiver);

			pointer.transform.parent = null;
		}

		if (lHandState == "open" && Within_screen (leftHandPos)) {
			movement_type = "";
		}


		//// Posicionamento e orientaçao do marcador 
		Position_marker_1 ();
		Position_marker_2 ();


		////Manipulaçao do objecto
		// Se a mao esquerda estiver fechada sera feita a rotaçao do objecto

		if (lHandState == "closed" && Target == null && rHandState!= "closed"){

			RaycastHit hitInfo;
			Target = GetClickedObject (out hitInfo, p_l) ;

			if(Target != null){
				Target.GetComponent<BoundBoxes_BoundBox>().enabled = true;
				offset_left = leftHandPos;
				Set_measure_bar();
				rotate_reset = Time.time + 1f;
			}
		}

		// Se estiver a "agarrar" vou rodar o objecto
		if (lHandState == "closed" && Target != null && rHandState != "closed" && funcao != "Clipping Box" && !zooming) {

			if (Time.time > rotate_reset){
				offset_left = leftHandPos;
			}
			rotate_reset = Time.time + 1f;

			RaycastHit hitInfo;
			GameObject nTarget = GetClickedObject_2 (out hitInfo, p_l);

			if(nTarget != null && movement_type==""){
				if(nTarget.tag == "axisX") movement_type = "X";
				if(nTarget.tag == "axisY") movement_type = "Y";
				if(nTarget.tag == "axisZ") movement_type = "Z";
			}

			if(Out_of_bouns_B(leftHandPos) && funcao=="" && movement_type=="" && !Widget_box(leftHandPos)){
				Target.transform.localRotation = initialrotation;
			}


			if(movement_type=="" && Within_screen(leftHandPos) && !Widget_box(leftHandPos) ){

				if(drag_rotate){
					Vector3 look = leftHandPos - last_left;
					look = look * 180f;
					Vector3 rot = new Vector3(look.y, -look.x, 0f);
					Target.transform.Rotate(rot, Space.World);
				}else{
					Vector3 look = leftHandPos - offset_left;
					look = look * - 50f;
					look.z = look.z * 2f;
					Target.transform.LookAt (look);
				}
			}
				if(movement_type=="X"){
					Vector3 look = leftHandPos - last_left;
					look = look * 180f;
					Vector3 rotate = new Vector3( look.y, 0f, 0f);
					Target.transform.Rotate(rotate, Space.Self);
				}
				if(movement_type=="Y"){
					Vector3 look = leftHandPos - last_left;
					look = look * 180f;
					Vector3 rotate = new Vector3( 0f, -look.x, 0f);
					Target.transform.Rotate(rotate, Space.Self);
				}
				if(movement_type=="Z"){
					Vector3 look = leftHandPos - last_left;
					look = look * 180f;
					Vector3 rotate = new Vector3( 0f, 0f, -look.x);
					Target.transform.Rotate(rotate, Space.Self);
				}


		}


		// Se a mao esquerda estiver em lasso sera feita a translaçao
		if(lHandState == "lasso" && Target == null){

			RaycastHit hitInfo;
			Target = GetClickedObject (out hitInfo, p_l) ;
			if(Target != null){
				Target.GetComponent<BoundBoxes_BoundBox>().enabled = true;
				offset_left = leftHandPos;
				Set_measure_bar();
			}
		}

		// Se estiver a "arrastar" vou mudar o objecto de posiçao
		if (lHandState == "lasso" && Target != null) {

			RaycastHit hitInfo;
			GameObject nTarget = GetClickedObject_2 (out hitInfo, p_l) ;
			
			if(nTarget != null && movement_type==""){
				if(nTarget.tag == "axisX") movement_type = "X";
				if(nTarget.tag == "axisY") movement_type = "Y";
				if(nTarget.tag == "axisZ") movement_type = "Z";
			}

			if(movement_type=="" && Within_screen(leftHandPos) && funcao != "Clipping Box" && !Widget_box(leftHandPos)){
				float movedz = leftHandPos.z - offset_left.z;
				float dist_z = camera.transform.position.z - Target.transform.position.z;
				Target.transform.position = camera.ScreenToWorldPoint(new Vector3(p_l.x, p_l.y, - dist_z + movedz*3f));
				offset_left = leftHandPos;
			}
			if(movement_type=="X"){
				Vector3 moved = leftHandPos - last_left;
				moved.y = 0f;
				moved.z = 0f;
				Target.transform.Translate(moved*2.5f, Space.Self);
			}
			if(movement_type=="Y"){
				Vector3 moved = leftHandPos - last_left;
				moved.x = 0f;
				moved.z = 0f;
				Target.transform.Translate(moved*2.5f, Space.Self);
			}
			if(movement_type=="Z"){
				Vector3 moved = leftHandPos - last_left;
				moved.x = 0f;
				moved.y = 0f;
				Target.transform.Translate(moved*2.5f, Space.Self);
			}
		}

		// Interacçao com menus 
		if (Out_of_bouns_L (leftHandPos) && !zooming && movement_type == "") {
			//print ("left out of bounds");
			panel_L.SetActive (true);

			float top = finalcoor.y;
			float bottom = initialcoor.y;

			float height = top-bottom;

			for(int i =0; i<4; i++){

				float bb = initialcoor.y + i*(height/4f);
				float tb = initialcoor.y + (i+1)*(height/4f);

				if( leftHandPos.y > bb && leftHandPos.y < tb){
					//print ("botao "+i);
					left_buttons[i].color = Color.yellow;
					if(lHandState=="closed") left_buttons[i].color = Color.red;
					if(lHandState=="closed" && i== 3 && Time.time > button_block){
						GameObject cursor = (GameObject)Instantiate (Resources.Load ("Grapher"));
						cursor.transform.position = initialposition;
						cursor.transform.localRotation = initialrotation;
						button_block = Time.time + 0.5f;
					}
					if(lHandState=="closed" && i== 2 && Time.time > button_block){
						GameObject new_model = (GameObject)Instantiate (Target);
						new_model.transform.position = initialposition;
						button_block = Time.time + 0.5f;
					}
					if(lHandState=="closed" && i== 1 && Time.time > button_block){
						drag_rot_toggle();
						button_block = Time.time + 0.5f;
					}
					if(lHandState=="closed" && i== 0 && Time.time > button_block){
						toggle_frame();
						button_block = Time.time + 0.5f;
					}
				}else{
					left_buttons[i].color = Color.white;
				}
			}


		} else {
			panel_L.SetActive(false);
		}

		if (Out_of_bouns_R (rightHandPos) && !zooming && movement_type=="") {
			//print ("right out of bounds");
			panel_R.SetActive (true);

			float top = finalcoor.y;
			float bottom = initialcoor.y;
			
			float height = top-bottom;
			
			for(int i =0; i<4; i++){
				
				float bb = initialcoor.y + i*(height/4f);
				float tb = initialcoor.y + (i+1)*(height/4f);
				
				if( rightHandPos.y > bb && rightHandPos.y < tb){
					//print ("botao "+i);
					right_buttons[i].color = Color.yellow;
					if(rHandState=="closed") right_buttons[i].color = Color.red;
					if(rHandState=="closed" && i==3 && Time.time > button_block && Target!=null){
						if(funcao=="Tag"){
							Deselection();
						}else{
							Deselection();
							//print (Target);
							func_Tatget = Target;
							pointer.SetActive(true);
							pointer.transform.position = initialposition;
							Target= pointer;
							funcao = "Tag";
							funcao_display.text = funcao;
						}
						button_block = Time.time + 0.5f;
					}
					if(rHandState=="closed" && i==2 && Time.time > button_block && Target!=null){
						if(funcao=="Clipping Plane"){
							Deselection();
						}else{
							Deselection();
							func_Tatget = Target;
							Clipping_plane.SetActive(true);
							Clipping_plane.transform.position = initialposition;
							Target = Clipping_plane;
							funcao = "Clipping Plane";
							funcao_display.text = funcao;
						}
						button_block = Time.time + 0.5f;
					}
					if(rHandState=="closed" && i==1 && Time.time > button_block && Target!=null){
						if(funcao=="angle"){
							Deselection();
						}else{
							Deselection();
							func_Tatget = Target;
							pointer.SetActive(true);
							pointer.transform.position = initialposition;
							Target= pointer;
							funcao = "angle";
							funcao_display.text = funcao;
						}
						button_block = Time.time + 0.5f;
					}
					if(rHandState=="closed" && i==0 && Time.time > button_block && Target!=null){
						if(funcao=="Clipping Box"){
							Deselection();
						}else{
							Deselection();
							func_Tatget = Target;
							pointer.SetActive(true);
							pointer.transform.position = initialposition;
							Target= pointer;
							funcao = "Clipping Box";
							funcao_display.text = funcao;

							//
							Target.transform.rotation = func_Tatget.transform.rotation;
							target_scale.x = func_Tatget.transform.localScale.x;
							target_scale.y = func_Tatget.transform.localScale.y;
							target_scale.z = func_Tatget.transform.localScale.z;
							pointer.transform.parent = func_Tatget.transform;
							Vector3 new_pointer_pos = pointer.transform.localPosition;
							new_pointer_pos.x = target_scale.x/2f;
							new_pointer_pos.y = target_scale.y/2f;
							new_pointer_pos.z = -target_scale.z/2f;
							pointer.transform.localPosition = new_pointer_pos;
							pointer.transform.parent = null;
						}
						button_block = Time.time + 0.5f;
					}
				}else{
					right_buttons[i].color = Color.white;
				}
			}

		} else {
			panel_R.SetActive(false);
		}

		if (Out_of_bouns_B (leftHandPos)) {

			RaycastHit hitInfo;
			GameObject looking = GetClickedObject (out hitInfo, p_l) ;

			if( lHandState == "lasso" && Target == looking && Target!= pointer && Target!= Clipping_plane){
				GameObject.Destroy(Target);
				Target = null;
			}

		}

		last_left = leftHandPos;

	}

	// Definir o "ecra" em relaçao a pessoa. Este ecra e definido no espaço real

	void Define_screen(){

		Vector3 center_coor = (leftShoulderPos + rightShoulderPos) / 2f;
		//print (center_coor);

		float unit_l = Mathf.Abs (leftShoulderPos.x - rightShoulderPos.x);
		float unit_h = Mathf.Abs (headPos.y - center_coor.y);

		initialcoor = center_coor;
		initialcoor.x = initialcoor.x - screen_proportion * unit_l;
		initialcoor.y = initialcoor.y - screen_proportion * unit_h;
		//print (initialcoor);

		finalcoor = center_coor;
		finalcoor.x = finalcoor.x + screen_proportion * unit_l;
		finalcoor.y = finalcoor.y + screen_proportion * unit_h;
		//print (finalcoor);

		screen_wide = finalcoor.x - initialcoor.x;
		screen_high = finalcoor.y - initialcoor.y;
	}

	bool Widget_box (Vector3 hand){

		float top = camera.pixelHeight * 0.27f;
		float right = camera.pixelWidth * 0.15f;
		Vector2 hand_p = Screen_positions (hand);
		
		if (hand_p.x < right && hand_p.y < top) {
			return true;
		} else {
			return false;
		}
	}

	bool Within_screen(Vector3 hand){

		float top = finalcoor.y;
		float bottom = initialcoor.y;
		float left = initialcoor.x;
		float right = finalcoor.x;

		if (hand.x > left && hand.x < right && hand.y > bottom && hand.y < top) {
			return true;
		} else {
			return false;
		}
	}

	bool Out_of_bouns_R(Vector3 rhand){

		float top = finalcoor.y;
		float bottom = initialcoor.y;
		float right = finalcoor.x;
		
		if (rhand.x > right && rhand.y > bottom && rhand.y < top) {
			return true;
		} else {
			return false;
		}
	}

	bool Out_of_bouns_L(Vector3 lhand){
		
		float top = finalcoor.y;
		float bottom = initialcoor.y;
		float left = initialcoor.x;
		
		if (lhand.x < left && lhand.y > bottom && lhand.y < top) {
			return true;
		} else {
			return false;
		}
	}

	bool Out_of_bouns_B(Vector3 lhand){
		
		float bottom = initialcoor.y + screen_high * 0.1f;
		float left = initialcoor.x;
		float right = finalcoor.x;
		
		if (lhand.x > left && lhand.x < right && lhand.y < bottom) {
			return true;
		} else {
			return false;
		}
	}

	bool Out_of_bouns_U(Vector3 lhand){
		
		float top = finalcoor.y;
		float left = initialcoor.x;
		float right = finalcoor.x;
		
		if (lhand.x > left && lhand.x < right && lhand.y > top) {
			return true;
		} else {
			return false;
		}
	}

	Vector2 Screen_positions (Vector3 hand){

		float x = ((hand.x - initialcoor.x) / screen_wide) * camera.pixelWidth;
		float y = ((hand.y - initialcoor.y) / screen_high) * camera.pixelHeight;

		Vector2 p = new Vector2 (x, y);

		return p;
	}

	GameObject GetClickedObject (out RaycastHit hit, Vector2 hand_pos)
	{
		GameObject target = null;
		Ray ray = camera.ScreenPointToRay (hand_pos);
		if (Physics.Raycast (ray.origin, ray.direction * 10, out hit)) {
			target = hit.collider.gameObject;
		}
		
		return target;
	}

	GameObject GetClickedObject_2 (out RaycastHit hit, Vector2 hand_pos)
	{
		GameObject target = null;
		Ray ray = widget_camera.ScreenPointToRay (hand_pos);
		if (Physics.Raycast (ray.origin, ray.direction * 10, out hit)) {
			target = hit.collider.gameObject;
		}
		
		return target;
	}


	bool Detect_right_angle(Vector3 wrist, Vector3 elbow, Vector3 shoulder, float angle_threshold){

		Vector3 forearm = wrist - elbow;
		Vector3 arm = shoulder - elbow;

		//Fazer em 2D
		forearm.x = 0f;
		arm.x = 0f;

		float angle = Vector3.Angle(forearm, arm);
		//print ("angulo do cotovelo esquerdo " + angle);

		if (angle < 90f + angle_threshold && angle > 90f - angle_threshold) {
			return true;
		} else {
			return false;
		}

	}

	// E preciso a ponta da mao para funcionar bem
	bool Left_arm_flat (float angle_threshold){

		//Vector3 one = leftHandPos - leftWristPos;
		Vector3 one = new Vector3 (0f, 1f, 0f);
		Vector3 two = leftElbowPos - leftWristPos;
		two.x = 0f;
		float angle = Vector3.Angle(one, two);
		//print ( "angulo braço esquerdo " + angle);

		if (angle > 90f - angle_threshold && angle < 90f + angle_threshold) {
			return true;
		} else {
			return false;
		}
	}

	bool Right_arm_flat (float angle_threshold){
		
		//Vector3 one = rightHandPos - rightWristPos;
		Vector3 one = new Vector3 (0f, 1f, 0f);
		Vector3 two = rightElbowPos - rightWristPos;
		two.x = 0f;
		float angle = Vector3.Angle(one, two);
		//print ( "angulo braço direito " + angle);
		
		if (angle > 90f - angle_threshold && angle < 90f + angle_threshold) {
			return true;
		} else {
			return false;
		}
	}

	bool Clap ( float clap_thresh){

		Vector3 distance = leftHandPos - rightHandPos;
		float reference = Mathf.Abs (leftShoulderPos.x - rightShoulderPos.x);
		float dist_mag = distance.magnitude / reference ;

		if (dist_mag < clap_thresh) {
			return true;
		} else {
			return false;
		}
	}

	void Position_marker_1(){

		if (Target == null) {
			marker_1.transform.position = camera_object.transform.position - new Vector3 (0, 0, 1f);
			marker_1.transform.localRotation = initialrotation;
		} else {
			float y_offset = 0.8f + Target.transform.localScale.y/2;
			marker_1.transform.position = Target.transform.position + new Vector3(0,y_offset,0);
			marker_1.transform.localRotation = Target.transform.localRotation;
		}
	}

	void Position_marker_2(){

		if (func_Tatget == null) {
			marker_2.transform.position = camera_object.transform.position - new Vector3 (0, 0, 1f);
		} else {
			float y_offset = - 0.6f - func_Tatget.transform.localScale.y/2;
			marker_2.transform.position = func_Tatget.transform.position + new Vector3(0,y_offset,0);
		}
	}

	void Set_measure_bar(){

		if(Target!= null){
			Grapher5Up grafer = Target.GetComponent<Grapher5Up> ();
			float dist_ref;
			if(grafer==null){
				dist_ref = 25f;
				scale = 1f;
			}else{
				dist_ref = grafer.dist_ref;
				scale = grafer.scale;
			}
			float scaled_ref = 5f / dist_ref;
			scaled_ref = scaled_ref / 0.2f;
			Vector3 new_scale = new Vector3 (scaled_ref * scale, scale_bar.transform.localScale.y, scale_bar.transform.localScale.z);
			scale_bar.transform.localScale = new_scale;
		}
	}

	void Tag_direction(){

		GameObject[] tags = GameObject.FindGameObjectsWithTag("measure");

		foreach (GameObject tag in tags) {

			Vector3 corrected = camera_object.transform.position;
			corrected.z *= -1f;
			tag.transform.LookAt (corrected);
		}
	}

	void Deselection(){

		funcao = "";
		funcao_display.text = funcao;
		if (func_Tatget!= null) Target = func_Tatget;
		func_Tatget = null;
		pointer.SetActive(false);
		Clipping_plane.SetActive(false);

		if (first_marker != null) {
			GameObject.Destroy(first_marker);
			first_marker = null;
		}

		if (angle_1!= null) {
			GameObject.Destroy(angle_1);
			angle_1 = null;
		}

		if (angle_2 != null) {
			GameObject.Destroy(angle_2);
			angle_2= null;
		}

		if (angle_line_1 != null) {
			GameObject.Destroy(angle_line_1);
			angle_line_1= null;
		}
	}

	private void drag_rot_toggle(){
		drag_rotate = !drag_rotate;
	}

	public void toggle_frame(){
	
		if (frame.activeSelf) {
			frame.SetActive (false);
		} else {
			frame.SetActive(true);
		}

	}

}
