﻿using UnityEngine;
using System.Collections;

public class Frame_control : MonoBehaviour {

	
	public Testmovements main_control;

	public Camera main_camera;

	// Use this for initialization
	void Start () {

		Set_measures ();
	
	}
	
	// Update is called once per frame
	void Update () {
	
		Set_position ();

		BoundBoxes_BoundBox box = GetComponent<BoundBoxes_BoundBox>();

		if (Check_measure ()) {

			box.lineColor = Color.red;

		} else {

			box.lineColor = Color.green;
		}
	}


	void Set_measures(){

		float height_real = main_control.screen_height_real;
		float lenght_real = main_control.screen_length_real;

		float height_scale = height_real / 25f;
		float lenght_scale = lenght_real / 25f;

		transform.localScale = new Vector3 (lenght_scale, height_scale, 0.001f);

	}

	void Set_position(){

		if (main_control.Target != null) {
			Vector3 frame_pos = main_camera.transform.position;
			frame_pos.z = main_control.Target.transform.position.z;
			transform.position = frame_pos;
		} else {
			transform.position = main_camera.transform.position + new Vector3 (0f, 0f, 1f);
		}
	}

	bool Check_measure(){

		Vector3 upper_right = transform.position + new Vector3 (transform.localScale.x / 2f, transform.localScale.y / 2f, 0f);
		Vector3 lower_left = transform.position - new Vector3 (transform.localScale.x / 2f, transform.localScale.y / 2f, 0f);

		Vector3 screen_ur = main_camera.WorldToScreenPoint (upper_right);
		Vector3 screen_ll = main_camera.WorldToScreenPoint (lower_left);

		Vector3 screentop = new Vector3 (main_camera.pixelWidth, main_camera.pixelHeight, 0f);
		Vector3 screenbot = new Vector3 (0f, 0f, 0f);

		float dist_1 = Mathf.Abs (screen_ur.x - screen_ll.x);
		float dist_2 = Mathf.Abs (screentop.x - screenbot.x);

		print (dist_1 + " " + dist_2);

		if (Mathf.Abs (dist_1 - dist_2) < dist_2 * 0.05f ) {
			return true;
		} else {
			return false;
		}
	}
}
