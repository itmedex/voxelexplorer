﻿using UnityEngine;
using System.Collections;

/// <summary>
/// A 3D widget with 6 faces, that lets the user choose a direction (view) for a camera.
/// 
/// This widget must be a child of an object with a Camera, and the two objects must be in the same layer.
/// Each different camera must be in its own separate layer (example: layers Cam1, Cam2, Cam3, Cam4).
/// If the widgets are not in different layers, a click in one camera might activate the widget of another camera.
/// </summary>
public class CameraWidget : MonoBehaviour
{
    // Camera of this GameObject's parent, not of this gameObject
    public Camera parentCamera;

    public delegate void CameraWidgetAction(Camera camera, string view, Vector3 direction);
    public event CameraWidgetAction OnCameraChanged;

    // All the 7 children of the object: U D L R F B and 3D
    private GameObject[] parts;

    /// <summary>
    /// If true, this widget's camera is a 3D view, and the user will not be able to edit the model on this camera.
    /// Instead, the camera can orbit around the object. This is specific to Multibody Sketcher.
    /// </summary>
    public bool is3DView;

    void Start()
    {
        parentCamera = transform.parent.GetComponent<Camera>();

        parts = new GameObject[transform.childCount];
        if (transform.childCount != 7)
            Debug.LogError("CameraWidget " + name + " does not have exactly 7 children!");

        int i = 0;
        foreach (Transform child in transform)
            parts[i++] = child.gameObject;
    }

    void Update()
    {
        //if (Helper.GetInputDown(out isTouch, out ignored) && parentCamera.enabled && OnCameraChanged != null)
        if (TuioInput.touchCount > 0)
        {
            Touch touch = TuioInput.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                // See where the user clicked, but only in this layer
                Ray ray = parentCamera.ScreenPointToRay(touch.position);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << gameObject.layer))
                {
                    FireCameraChanged(hit.collider.gameObject.name);
                }
            }
        }
    }

    /// <summary>
    /// Fires a OnCameraChanged event, just like if the user had clicked on the CameraWidget.
    /// Useful to set the cameras programmatically, like at the start of the app.
    /// </summary>
    /// <param name="view"></param>
    public void FireCameraChanged(string view)
    {
        // De-highlight all parts, highlight the clicked one
        foreach (GameObject part in parts)
            part.GetComponent<Highlight>().SetHighlight(part.name == view);

        if (OnCameraChanged == null)
            return;

        is3DView = (view == "3D");

        switch (view)
        {
            case "U": OnCameraChanged(parentCamera, "U", Vector3.up); break;
            case "D": OnCameraChanged(parentCamera, "D", Vector3.down); break;
            case "L": OnCameraChanged(parentCamera, "L", Vector3.left); break;
            case "R": OnCameraChanged(parentCamera, "R", Vector3.right); break;
            case "F": OnCameraChanged(parentCamera, "F", Vector3.back); break;
            case "B": OnCameraChanged(parentCamera, "B", Vector3.forward); break;
            case "3D": OnCameraChanged(parentCamera, "3D", new Vector3(-1, -1, 1)); break;
        }
    }
}
